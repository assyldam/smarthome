# Smart Home
### Projekt do předmětu B6B36OMO - Objektový návrh a modelování
### Autoři: Kabi Damir, Nurakhmetov Alisher
### Vyučující: Ing. Daniel Groschup, Ing. Jan Zubr

## Použité design patterny:
#### Observer - package cz.cvut.fel.omo.semestralka.observers
#### Visitor - package cz.cvut.fel.omo.semestralka.visitors
#### Builder - package cz.cvut.fel.omo.semestralka.builders
#### Factory - package cz.cvut.fel.omo.semestralka.factories
#### Chain of responsibility - package cz.cvut.fel.omo.semestralka.handlers
#### State machine - package cz.cvut.fel.omo.semestralka.devicetypes.devicestate
#### Facade - cz.cvut.fel.omo.semestralka.facade
#### Singleton - package cz.cvut.fel.omo.semestralka.locations.Home
#### Object pool - package cz.cvut.fel.omo.semestralka.pools
#### Stream - package cz.cvut.fel.omo.semestralka.controller

## Funkční požadavky

#### F1- Splněno.

#### F2- Splněno, každé zařízení se může zapnout, vypnout, dát do nečinného stavu, rozbít a opravit. Jednotlivá zařízení v domu mají API na ovládání stavů.

#### F3- Splněno, při změně stavu se spočítá spotřeba v aktuálním stavu.

#### F4- Splněno, sbíráme data o spotřebě elektřiny a počítáme cenu ni.

#### F5- Splněno, Osoba může používat zařízení, najíst se (pokud bude hladová), dále může krmit děti nebo zvířata (pokud budou hladoví), může jít na kolo nebo vzít auto (pokud v něm bude ještě volné místo) a jít lyžovat, osoba může také opravit zařízení.

#### F6- Splněno package controllers.

#### F7- Splněno package controllers + handlers.

#### F8- Splněno, složka reports, EventReport a ActivityAndUsageReport jsou v jednom souboru.

#### F9- Splněno, handlers.BrokenDeviceHandler, deviceStates.OffState - getFixed.

#### F10- Splněno, controller.HouseController

## Nefunkční požadavky

#### Není požadována autentizace ani autorizace.

#### Aplikace může běžet pouze v jedné JVM.

#### Javadoc.

#### Reporty jsou ve složce reports.

#### Konfigurace jsou v src/main/recources/