
---------------- STARTING NEW SIMULATION ----------------
                     15.2.2024 12:09                       

Rooms info: 
   Attic - Bulb named light is free: false, SensorHumid (Sensor), 
   Kitchen - Bulb named light1 is free: false, 
   Kitchen - Fridge named fridge is free: false, SensorLight (Sensor), 
   Bedroom - Bulb named light3 is free: false, SensorTemp (Sensor), 
   Study - Bulb named light4 is free: false, SensorHumid (Sensor), 
   Gaming room - Bulb named light2 is free: false


 - ROUND 1 STARTED: -

Mom did actions: use device: light3
Jack did actions: Drifting.
Father did actions: go cycling.
Rose did actions: use device: light1
Aunt did actions: take car and go skiing.
Son did actions: use device: light2

Home parameters: -windspeed: 0, -temp: 0, -humid: 0, -brightness: 0

 - ROUND 2 STARTED: -

Jack did actions: use device: light1
Father did actions: use device: light2
Mom did actions: use device: light3
Son did actions: use device: light4
Rose did actions: use device: light
Device activity was not found, trying to find another activity for person.
Aunt did actions: Drifting.

Home parameters: -windspeed: 0, -temp: 0, -humid: 0, -brightness: 0

 - ROUND 3 STARTED: -

Child Son did actions: become hungry.
Mom did actions: use device: light
Father did actions: go cycling.
Jack did actions: use device: light2
Rose did actions: Drifting.
Aunt did actions: take car and go skiing.

Home parameters: -windspeed: 12, -temp: 29, -humid: 64, -brightness: 58

 - ROUND 4 STARTED: -

Son did actions: be fed by Mom.
Mom did actions: Feed child Son with some food in the kitchen.
Jack did actions: Eat some food found in the kitchen.
Father did actions: use device: light2
Aunt did actions: use device: light4
Rose did actions: take car and go skiing.
Mom did actions: use device: light
Son did actions: go cycling.

Home parameters: -windspeed: 12, -temp: 29, -humid: 64, -brightness: 58

 - ROUND 5 STARTED: -

Aunt did actions: go cycling.
Mom did actions: use device: light3
Father did actions: take car and go skiing.
Jack did actions: Drifting.
Sport activity was not found, trying to find another activity for person.
Rose did actions: use device: light
Son did actions: use device: light1

Home parameters: -windspeed: 12, -temp: 29, -humid: 64, -brightness: 83

 - ROUND 6 STARTED: -

Child Son did actions: become hungry.
Aunt did actions: go cycling.
Mom did actions: take car and go skiing.
Rose did actions: Drifting.
Sport activity was not found, trying to find another activity for person.
Father did actions: use device: light3
Jack did actions: use device: light

Home parameters: -windspeed: 19, -temp: 16, -humid: 52, -brightness: 14

 - ROUND 7 STARTED: -

Son did actions: be fed by Aunt.
Aunt did actions: Feed child Son with some food in the kitchen.
Pet Gard did actions: become hungry.
Son did actions: go cycling.
Mom did actions: Drifting.
Rose did actions: take car and go skiing.
Jack did actions: use device: light3
Aunt did actions: use device: light
Father did actions: use device: light1

Home parameters: -windspeed: 19, -temp: 16, -humid: 52, -brightness: 39

 - ROUND 8 STARTED: -

Gard did actions: be fed by Jack.
Jack did actions: Feed pet Gard with some food in the kitchen.
Father did actions: Eat some food found in the kitchen.
Son did actions: use device: light
Mom did actions: use device: light2
Jack did actions: take car and go skiing.
Aunt did actions: use device: light3
Rose did actions: Drifting.

Home parameters: -windspeed: 19, -temp: 16, -humid: 52, -brightness: 64

 - ROUND 9 STARTED: -

Child Son did actions: become hungry.
Aunt did actions: Drifting.
Jack did actions: take car and go skiing.
Mom did actions: use device: light1
Rose did actions: go cycling.
Father did actions: use device: light4

Home parameters: -windspeed: 2, -temp: 13, -humid: 31, -brightness: 44

 - ROUND 10 STARTED: -

Son did actions: be fed by Rose.
Rose did actions: Feed child Son with some food in the kitchen.
Father did actions: use device: light3
Mom did actions: Drifting.
Son did actions: use device: light
Aunt did actions: take car and go skiing.
Jack did actions: go cycling.
Rose did actions: use device: light1

Home parameters: -windspeed: 2, -temp: 13, -humid: 31, -brightness: 69

 - ROUND 11 STARTED: -

Rose did actions: Drifting.
Father did actions: use device: light4
Son did actions: use device: light3
Jack did actions: take car and go skiing.
Aunt did actions: go cycling.
Mom did actions: use device: light1

Home parameters: -windspeed: 2, -temp: 13, -humid: 31, -brightness: 89

 - ROUND 12 STARTED: -

Child Son did actions: become hungry.
Aunt did actions: Eat some food found in the kitchen.
Father did actions: go cycling.
Mom did actions: take car and go skiing.
Jack did actions: Drifting.
Sport activity was not found, trying to find another activity for person.
Rose did actions: use device: light2

Home parameters: -windspeed: 14, -temp: 20, -humid: 52, -brightness: 42

 - ROUND 13 STARTED: -

Son did actions: be fed by Mom.
Mom did actions: Feed child Son with some food in the kitchen.
Son did actions: use device: light4
Rose did actions: take car and go skiing.
Jack did actions: use device: light2
Father did actions: use device: light
Mom did actions: Drifting.
Aunt did actions: use device: light3

Home parameters: -windspeed: 14, -temp: 20, -humid: 52, -brightness: 62

 - ROUND 14 STARTED: -

light1 device is broken.
Rose did actions: repair the light1.
Pet Gard did actions: become hungry.
Jack did actions: take car and go skiing.
Aunt did actions: use device: light
Rose did actions: Drifting.
Father did actions: use device: light3
Son did actions: go cycling.
Mom did actions: use device: light4

Home parameters: -windspeed: 14, -temp: 20, -humid: 52, -brightness: 82

 - ROUND 15 STARTED: -

Gard did actions: be fed by Jack.
Jack did actions: Feed pet Gard with some food in the kitchen.
Child Son did actions: become hungry.
Father did actions: use device: light
Jack did actions: take car and go skiing.
Mom did actions: use device: light2
Aunt did actions: go cycling.
Rose did actions: Drifting.

Home parameters: -windspeed: 10, -temp: 19, -humid: 36, -brightness: 74

 - ROUND 16 STARTED: -

Son did actions: be fed by Jack.
Jack did actions: Feed child Son with some food in the kitchen.
Father did actions: Eat some food found in the kitchen.
Rose did actions: Drifting.
Aunt did actions: go cycling.
Jack did actions: take car and go skiing.
Sport activity was not found, trying to find another activity for person.
Son did actions: use device: light3
Sport activity was not found, trying to find another activity for person.
Mom did actions: use device: light2

Home parameters: -windspeed: 10, -temp: 19, -humid: 36, -brightness: 94

 - ROUND 17 STARTED: -

light device is broken.
Father did actions: repair the light.
fridge device is broken.
Mom did actions: repair the fridge.
light3 device is broken.
Jack did actions: repair the light3.
light4 device is broken.
Rose did actions: repair the light4.
light2 device is broken.
Aunt did actions: repair the light2.
Rose did actions: use device: light1
Father did actions: Drifting.
Mom did actions: go cycling.
Aunt did actions: use device: light3
Son did actions: use device: light
Jack did actions: take car and go skiing.

Home parameters: -windspeed: 10, -temp: 19, -humid: 36, -brightness: 114

 - ROUND 18 STARTED: -

Child Son did actions: become hungry.
Father did actions: use device: light4
Rose did actions: go cycling.
Jack did actions: take car and go skiing.
Aunt did actions: Drifting.
Sport activity was not found, trying to find another activity for person.
Mom did actions: use device: light2

Home parameters: -windspeed: 12, -temp: 27, -humid: 28, -brightness: 86

 - ROUND 19 STARTED: -

Son did actions: be fed by Aunt.
Aunt did actions: Feed child Son with some food in the kitchen.
Jack did actions: use device: light1
Rose did actions: Drifting.
Sport activity was not found, trying to find another activity for person.
Son did actions: use device: light
Father did actions: go cycling.
Mom did actions: use device: light2
Sport activity was not found, trying to find another activity for person.
Aunt did actions: use device: light4

Home parameters: -windspeed: 12, -temp: 27, -humid: 28, -brightness: 106

 - ROUND 20 STARTED: -

Jack did actions: Eat some food found in the kitchen.
Son did actions: use device: light3
Aunt did actions: take car and go skiing.
Mom did actions: Drifting.
Father did actions: use device: light4
Rose did actions: use device: light2

Home parameters: -windspeed: 12, -temp: 27, -humid: 28, -brightness: 126

--------------- END OF THE SIMULATION ----------------

