――――――――――――――――――――――― REPORT BEGINNING ―――――――――――――――――――――――
                        15.2.2024 02:22                       
Home 
    Room -LivingRoom- has
      Device Door named Door
      Device AirConditioner named AirConditioner
      Device Bulb named LightInLivingRoom1
      Device Door named Door
      Sensor SensorTemp
      Sensor SensorLight
    Room -StudyRoom- has
      Device Humidifier named Humidifier
      Device Bulb named LightInStudyRoom1
      Device Bulb named LightInStudyRoom2
      Device Speaker named AlexaSpeaker
      Device Window named Window
      Device Door named Door
      Sensor SensorHumid
      Sensor SensorLight
      Sensor SensorWind
    Room -BedRoom- has
      Device Bulb named LightInBedRoom1
      Device Bulb named LightInBedRoom2
      Device Humidifier named Humidifier
      Device Window named Window
      Device Door named Door
      Sensor SensorHumid
      Sensor SensorLight
      Sensor SensorWind
    Room -BathRoom- has
      Device Bulb named LightInBathRoom1
      Device Bulb named LightInBathRoom2
      Device Humidifier named Humidifier
      Device Door named Door
      Sensor SensorHumid
      Sensor SensorLight
    Room -Kitchen- has
      Device Fridge named Fridge
      Device Window named Window
      Device Speaker named SiriSpeaker
      Device Bulb named LightInKitchen
      Device Door named Door
      Device Humidifier named Humidifier
      Sensor SensorLight
      Sensor SensorHumid
    Room -Garage- has
      Device Door named Door
      Device Bulb named LightInGarage
      Device Humidifier named Humidifier
      Sensor SensorLight
      Sensor SensorHumid


=== === === === === === === === === === === === ===


Number of occupants: 6
    Hi, I'm Darek and I'm 10 years old.
    Hi, I'm Mary and I'm 35 years old.
    Hi, I'm Frederick and I'm 35 years old.
    Hi, I'm Kate and I'm 27 years old.
    Hi, I'm Abby and I'm 15 years old.
    Hi, I'm Sara and I'm 52 years old.


=== === === === === === === === === === === === ===


Number of pets: 3
    This is a Dog named Rex.
    This is a Cat named Tom.
    This is a Mouse named Jerry.


=== === === === === === === === === === === === ===


Number of items: 4
    Item Skis
    Item Bike
    Item Bike
    Item Car


―――――――――――――――――――――――― REPORT END ――――――――――――――――――――――――
