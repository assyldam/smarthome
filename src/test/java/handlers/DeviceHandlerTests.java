package handlers;

import cz.cvut.fel.omo.semestralka.controller.Event;
import cz.cvut.fel.omo.semestralka.devicetypes.Camera;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.Fridge;
import cz.cvut.fel.omo.semestralka.devicetypes.Speaker;
import cz.cvut.fel.omo.semestralka.enums.Devices;
import cz.cvut.fel.omo.semestralka.handlers.DeviceHandler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

import java.util.Arrays;

public class DeviceHandlerTests {
    @Mock
    private Fridge fridgeMock;
    @Mock
    private Speaker speakerMock;
    @Mock
    private Camera cameraMock;

    private DeviceHandler deviceHandler;

    @Mock
    private Event eventMock;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        deviceHandler = new DeviceHandler();
        when(eventMock.getResponsibleDevice()).thenReturn(fridgeMock);
    }

    @Test
    public void testProcessEventWithFridgeHavingFood() {
        List<String> foodList = Arrays.asList("Apple", "Banana");
        when(fridgeMock.getType()).thenReturn(Devices.Fridge);
        when(fridgeMock.getContent()).thenReturn(new ArrayList<>(foodList));

        when(eventMock.getPet()).thenReturn(null);
        when(eventMock.getChild()).thenReturn(null);

        deviceHandler.processEvent(eventMock);

        verify(fridgeMock, times(1)).removeFood(anyString());
    }

    @Test
    public void testProcessEventWithSpeaker() {
        when(speakerMock.getType()).thenReturn(Devices.Speaker);
        when(eventMock.getResponsibleDevice()).thenReturn(speakerMock);

        deviceHandler.processEvent(eventMock);

        verify(eventMock, times(1)).setWhatToDo(contains("use Speaker"));
    }

    @Test
    public void testProcessEventWithCamera() {
        when(cameraMock.getType()).thenReturn(Devices.Camera);
        when(eventMock.getResponsibleDevice()).thenReturn(cameraMock);

        deviceHandler.processEvent(eventMock);

        verify(eventMock, times(1)).setWhatToDo(contains("shoot something in camera"));
    }

    @Test
    public void testProcessEventGenericDevice() {
        // Assuming 'Device' is a generic device type for this test
        Device genericDeviceMock = mock(Device.class);
        when(genericDeviceMock.getType()).thenReturn(Devices.Bulb); // Example of another device type
        when(eventMock.getResponsibleDevice()).thenReturn(genericDeviceMock);

        deviceHandler.processEvent(eventMock);

        verify(eventMock, times(1)).setWhatToDo(contains("use device:"));
    }
}
