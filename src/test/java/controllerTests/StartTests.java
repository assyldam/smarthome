package controllerTests;

import static org.mockito.Mockito.*;

import cz.cvut.fel.omo.semestralka.controller.HomeController;
import cz.cvut.fel.omo.semestralka.controller.Start;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

public class StartTests {

    @Mock
    private Home homeMock;

    @Mock
    private HomeController homeControllerMock;

    private Start start;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        homeMock = mock(Home.class);
        when(homeControllerMock.getHome()).thenReturn(homeMock);
        ArrayList<Room> roomsMock = new ArrayList<>();
        when(homeMock.getRooms()).thenReturn(roomsMock);
        start = new Start(homeControllerMock.getHome()); // Assuming Start accepts a HomeController
    }

    @Test
    public void testTurnOnAllDevices() {
        MockitoAnnotations.initMocks(this);
        homeMock = mock(Home.class);
        when(homeControllerMock.getHome()).thenReturn(homeMock);
        ArrayList<Room> roomsMock = new ArrayList<>();
        when(homeMock.getRooms()).thenReturn(roomsMock);
        start = new Start(homeControllerMock.getHome()); // Assuming Start accepts a HomeController
        start.doRounds(1);
        verify(homeMock, atLeastOnce()).getRooms();
    }
}
