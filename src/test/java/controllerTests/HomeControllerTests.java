package controllerTests;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.PersonActivity;
import cz.cvut.fel.omo.semestralka.animatedobjects.PersonState;
import cz.cvut.fel.omo.semestralka.controller.Event;
import cz.cvut.fel.omo.semestralka.controller.HomeController;
import cz.cvut.fel.omo.semestralka.locations.Home;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.util.List;

public class HomeControllerTests {

    private HomeController controller;
    private Home homeMock;
    private Person personMock;

    @Before
    public void setUp() {
        homeMock = mock(Home.class);
//        MockitoAnnotations.initMocks(this);
        controller = new HomeController(homeMock);
    }

    @Test
    public void testGenerateHomeStatsChangeAddsEvent() {
        controller.generateHomeStatsChange();
        List<Event> events = controller.getEvents();
        assertEquals(1, events.size());
    }

    @Test
    public void testGenerateEventForSinglePerson() {
        when(homeMock.getRandomPerson()).thenReturn(personMock).thenReturn(null);

        controller.generateEventsForFreePeople();

        verify(homeMock, atLeastOnce()).getRandomPerson();
        List<Event> events = controller.getEvents();
        assertEquals("Expected no generated events", 0, events.size());
    }

    @Test
    public void testGenerateHomeStatsChange() {
        doNothing().when(homeMock).accept(any());

        controller.generateHomeStatsChange();
        assert(controller.getEvents().size() == 1);
        verify(homeMock, times(1)).accept(any());
    }

    @Test
    public void testGenerateHungryEventForChild() {
        when(homeMock.getRandomChild()).thenReturn(new Person("test", PersonState.Adult));

        controller.generateHungryEventForChild();

        assert(controller.getEvents().size() == 1);
        verify(homeMock, times(1)).getRandomChild();
    }
}

