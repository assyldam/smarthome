package configurationsTests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.omo.semestralka.configurations.ConfigurationLoader;
import cz.cvut.fel.omo.semestralka.configurations.HomeConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ConfigurationLoaderTests {

    @InjectMocks
    private ConfigurationLoader configurationLoader;

    @Mock
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void loadConfiguration_validConfig_returnsHomeConfig(@TempDir Path tempDir) throws IOException {
        String validConfig = "{ \"home\": { \"rooms\": [] } }";
        File tempFile = tempDir.resolve("validConfig.json").toFile();

        java.nio.file.Files.writeString(tempFile.toPath(), validConfig);

        when(objectMapper.readTree(any(File.class))).thenReturn(mock(JsonNode.class));

        HomeConfig result = configurationLoader.loadConfiguration(tempFile.getAbsolutePath());

        assertNotNull(result, "HomeConfig should not be null for valid config");
    }

    @Test
    void loadConfiguration_missingHomeKey_returnsNull(@TempDir Path tempDir) throws IOException {
        String configWithoutHome = "{}";
        File tempFile = tempDir.resolve("configWithoutHome.json").toFile();
        Files.writeString(tempFile.toPath(), configWithoutHome);

        HomeConfig result = configurationLoader.loadConfiguration(tempFile.getAbsolutePath());

        assertNull(result, "ConfigurationLoader should return null for config missing 'home' key.");
    }

    @Test
    void loadConfiguration_invalidPath_returnsNull() {
        String invalidPath = "/path/does/not/exist/config.json";

        HomeConfig result = configurationLoader.loadConfiguration(invalidPath);

        assertNull(result, "ConfigurationLoader should return null for a non-existent file path.");
    }
}

