package cz.cvut.fel.omo.semestralka.animatedobjects;

import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;


/**
 * This class represents a person.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class Person  {
    private final String name;
    private Room room;
    protected boolean isHungry = false;
    private int age;
    private PersonState personState;
    private PersonActivity activity;

    public Person(String name, PersonState personState) {
        this.name = name;
        this.personState = personState;
    }

    /**return State of Person(Adult/Child)
     * @return PersonState
     * */
    public PersonState getPersonState() {
        return personState;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Room getRoom(){
        return room;
    }

    public String getName() {
        return name;
    }

    public PersonActivity getActivity() {
        return activity;
    }

    public void setActivity(PersonActivity value) {
        activity = value;
    }

    /**Performs visitor logic for Person class
     * @param visitor Visitor
     */
    public void accept(Visitor visitor){
        visitor.visit(this);
    }
}
