package cz.cvut.fel.omo.semestralka.locations;

import com.sun.jdi.connect.AttachingConnector;
import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.items.Item;
import cz.cvut.fel.omo.semestralka.pools.DevicePool;
import cz.cvut.fel.omo.semestralka.pools.ItemPool;
import cz.cvut.fel.omo.semestralka.pools.PersonPool;
import cz.cvut.fel.omo.semestralka.visitor.ChangeRoomStateVisitor;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Home extends StatsChangable{
    private double consumption;

    private static Home instance;

    private ArrayList<Room> rooms = new ArrayList<>();

    private ArrayList<Pet> pets = new ArrayList<>();

    private DevicePool devicePool = DevicePool.getInstance();

    private ItemPool itemPool = ItemPool.getInstance();

    private PersonPool personPool = PersonPool.getInstance();

    private ChangeRoomStateVisitor changeRoomStateVisitor;


    public Home(){
        this.rooms = new ArrayList<>();
        this.pets = new ArrayList<>();
        itemPool = ItemPool.getInstance();
        devicePool = DevicePool.getInstance();
        personPool = PersonPool.getInstance();
        changeRoomStateVisitor = new ChangeRoomStateVisitor(this);

    }

    public void setPets(ArrayList<Pet> pets) {
        this.pets = pets;
    }

    public void setConsumption(double consumption){
        this.consumption = consumption;
    }

    public double getConsumption(){
        int sum = 0;
        for (Room room: rooms){
            sum += room.getConsumption();
        }
        return sum;
    }

    public static Home getInstance(){
        if(instance == null){
            instance = new Home();
        }
        return instance;
    }

    /**Add room to home
     * @param room Room
     * @return Home
     */
    public Home addRoom(Room room) {
        this.rooms.add(room);
        return this;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void accept(Visitor visitor){
        visitor.visit(this);
    }

    public ArrayList<Pet> getPets() {
        return pets;
    }

    public ArrayList<Sensor> getSensors() {
        return rooms.stream()
                .flatMap(room -> room.getSensors().stream())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * return random person from pool
     * @return Person
     */
    public Person getRandomPerson(){return personPool.getRandomPerson();}

    /**
     * return random Device from pool
     * @return Device
     */
    public Device getRandomDeviceFromPool(){
        return devicePool.getRandomDevice();
    }


    /**
     * return Items from pool
     * @return Items
     */
    public ArrayList<Item> getItems(){
        return itemPool.getItems();
    }

    /**
     * return random Item from pool
     * @return Item
     */
    public Item getItemFromPool(){
        return itemPool.getRandomItem();
    }

    /**
     * release Item to pool
     */
    public void releaseItemToPool(Item item){
        itemPool.releaseItem(item);
    }

    /**
     * release Items to pool
     */
    public void releaseItemToPool(ArrayList<Item> items){
        for (Item i: items){
            itemPool.releaseItem(i);
        }
    }


    /**
     * release Device to pool
     */
    public void releaseDeviceToPool(Device device){
        devicePool.releaseDevice(device);
    }


    /**
     * release Devices to pool
     */
    public void releaseDeviceToPool(ArrayList<Device> devices){
        for (Device d: devices){
            devicePool.releaseDevice(d);
        }
    }


    /**
     * release Person to pool
     */
    public void releasePersonToPool(Person person){
        personPool.releasePerson(person);
    }


    /**
     * release Person to pool
     */
    public void releasePersonToPool(ArrayList<Person> person){
        for (Person p: person){
            personPool.releasePerson(p);
        }
    }


    /**
     * release Child to pool
     */
    public Person getRandomChild(){
        return personPool.getRandomChild();
    }


    /**
     * release Adult to pool
     */
    public Person getRandomAdult(){
        return personPool.getRandomAdult();
    }

    /**
     * Update Home Stats (depends on device modes)
     */
    public void updateHomeStats(){
        ArrayList<Device> homeStatsChangeDevices = new ArrayList<>();
        while(true){
            Device d = devicePool.getRandomDevice();
            if(d == null) break;
            switch (d.getType()){
                case AirConditioner, Bulb, Humidifier, Window -> {
                    homeStatsChangeDevices.add(d);
                    d.accept(changeRoomStateVisitor);
                }
            }
        }
        homeStatsChangeDevices.forEach(this::releaseDeviceToPool);
    }
}
