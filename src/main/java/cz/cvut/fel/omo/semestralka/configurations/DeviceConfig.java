package cz.cvut.fel.omo.semestralka.configurations;

import cz.cvut.fel.omo.semestralka.enums.State;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.enums.Devices;

/**
 * This class represents device configuration.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class DeviceConfig {
    private final String name;
    private final Devices type;
    private final double consumption;
    private final State state;

    public DeviceConfig(String name, Devices type, double consumption, boolean state) {
        this.name = name;
        this.type = type;
        this.consumption = consumption;
        if(state)this.state = State.ON;
        else this.state = State.OFF;
    }

    public String getName() {
        return name;
    }

    public Devices getType() {
        return type;
    }

    public double getConsumption() {
        return consumption;
    }
}
