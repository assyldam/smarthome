package cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;

/**
 * This interface represents level states of device.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public interface LevelState {
    /**
     * Execute on some level mode
     * @param device Device
     */
    public void execute(Device device);
}
