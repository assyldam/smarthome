package cz.cvut.fel.omo.semestralka.handlers;

import cz.cvut.fel.omo.semestralka.controller.Event;

public class WindowHandler extends AbstractHandler {

    /**Main event logic here
     * @param event Event
     */
    @Override
    public void processEvent (Event event) {
        if (event.getWhatToDo().contains("Open all windows.")) {
            event.getHome().getRooms().forEach(room ->
                    room.getWindows().forEach(window -> window.lowLevelProcess()));
        }

        if (event.getWhatToDo().contains("Close all windows.")) {
            event.getHome().getRooms().forEach(room ->
                    room.getWindows().forEach(window -> window.highLevelProcess()));
        }
    }
}
