package cz.cvut.fel.omo.semestralka.handlers;

import cz.cvut.fel.omo.semestralka.controller.Event;

public class BrokenDeviceHandler extends AbstractHandler{

    /**Main event logic here
     * @param event Event
     */
    @Override
    public void processEvent (Event event) {
        event.getResponsibleDevice().getFixed();
        event.getResponsibleDevice().turnOn();
        //event.getResponsibleDevice().execute();
    }
}
