package cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate;

import cz.cvut.fel.omo.semestralka.enums.Code;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;

public class PowerStateIdle implements PowerState {
    @Override
    public boolean isOn(){return true;}

    @Override
    public void execute(Device device) {
        device.turnOn();
    }
}
