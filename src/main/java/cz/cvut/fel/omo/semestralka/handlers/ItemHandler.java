package cz.cvut.fel.omo.semestralka.handlers;

import cz.cvut.fel.omo.semestralka.controller.Event;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemHandler extends AbstractHandler {

    private static final Logger LOG = LoggerFactory.getLogger(String.valueOf(ItemHandler.class));

    /**Main event logic here
     * @param event Event
     */
    @Override
    public void processEvent (Event event) {
        PersonHandler personHandler = new PersonHandler();
        setNextHandler(personHandler);

        if (event.getResponsibleItem().getName().equals("Skis")) {
            event.setWhatToDo("take car and go skiing.");
        }
        if (event.getResponsibleItem().getName().equals("Bike")) {
            event.setWhatToDo("go cycling.");
        }
        if (event.getResponsibleItem().getName().equals("Car")) {
            event.setWhatToDo("Drifting.");
        }
    }
}
