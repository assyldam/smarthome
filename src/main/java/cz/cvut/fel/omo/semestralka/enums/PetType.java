package cz.cvut.fel.omo.semestralka.enums;

public enum PetType {
    Cat,
    Dog,
    Fish,
    Bird,
    Mouse
}
