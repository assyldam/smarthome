package cz.cvut.fel.omo.semestralka.devicetypes.manual;

public class Manual {
    private String manText;
    public Manual(String manText){
        this.manText = manText;
    }

    public String getManText(){
        return manText;
    }
}
