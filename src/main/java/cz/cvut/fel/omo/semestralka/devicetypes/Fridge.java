package cz.cvut.fel.omo.semestralka.devicetypes;

import cz.cvut.fel.omo.semestralka.devicetypes.manual.Manual;
import cz.cvut.fel.omo.semestralka.enums.Code;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

import java.util.ArrayList;

public class Fridge extends Device {
    public Fridge() {
        super();
        manual = new Manual("Fridge manual");
    }

    public Fridge(String name) {
        super(name);
    }

    public Fridge(String name, double consumption) {
        super(name, consumption);
    }

    public ArrayList<String> getContent(){
        return new ArrayList<>();
    }

    public void removeFood(String randomFoodFromFridge) {
    }

    @Override
    public int getOutputPower() {
        return 0;
    }

    /**
     * High level mode implementation
     */
    @Override
    public void concreteHighLevelProcess() {
        consumption = defaultConsumption * 1.5;
        System.out.println(type + name + " works on high level mode with consumption: " + consumption);
    }

    /**
     * Mid level mode implementation
     */
    @Override
    public void concreteMidLevelProcess() {
        consumption = defaultConsumption;
        System.out.println(type + name + " works on mid level mode with consumption: " + consumption);
    }

    /**
     * Low level mode implementation
     */
    @Override
    public void concreteLowLevelProcess() {
        consumption = defaultConsumption / 2;
        System.out.println(type + name + " works on low level mode with consumption: " + consumption);
    }

    /**Method for visitor pattern
     */
    @Override
    public int accept(Visitor v) {
        return v.doForFridge(this);
    }
}
