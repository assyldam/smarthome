package cz.cvut.fel.omo.semestralka.pools;

import cz.cvut.fel.omo.semestralka.items.Item;

import java.util.ArrayList;
import java.util.Collections;

public class ItemPool {
    private final ArrayList<Item> items = new ArrayList<>();
    private static ItemPool INSTANCE;

    /**
     * return back Item to pool
     * @param item Item
     */
    public void releaseItem(Item item){
        items.add(item);
    }

    public static ItemPool getInstance(){
        if(INSTANCE != null) return INSTANCE;
        INSTANCE = new ItemPool();
        return INSTANCE;
    }


    /**
     * return Item from pool
     * @return Item
     */
    public Item getRandomItem(){
        if(!items.isEmpty()) {
            Collections.shuffle(items);
            Item tmp = items.get(0);
            items.remove(tmp);
            return tmp;
        }
        return null;
    }

    public ArrayList<Item> getItems(){
        return items;
    }
}
