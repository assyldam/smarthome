package cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.enums.Code;

public class PowerStateOff implements PowerState {
    @Override
    public boolean isOn(){
        return false;
    }

    /**Execute on "turn off" mode
     * @param device Device
     */
    @Override
    public void execute(Device device) {
        System.out.println("Device " + device.getName() + " is OFF");
    }
}
