package cz.cvut.fel.omo.semestralka.observers;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate.LevelState;
import cz.cvut.fel.omo.semestralka.enums.Code;

import java.util.ArrayList;

public abstract class Observer {
    protected ArrayList<Device> subscribers = new ArrayList<>();

    /**
     * THis method notify all subscribers
     * @param levelState Level State
     */
    public void notifyAllSubscribers(LevelState levelState){
        for(Device device : subscribers){
            device.setLevelState(levelState);
        }
    }

    /**
     * Add Subscribers to Observer
     * @param devices Devices
     */
    public abstract void addSubscribers(ArrayList<Device> devices);

    public void removeSubscriber(Device device){
        subscribers.remove(device);
    }

    public void removeSubscribers(ArrayList<Device> devices){
        subscribers.removeAll(devices);
    }

    public void removeSubscribers(){
        subscribers.clear();
    }

    public ArrayList<Device> getSubscribers(){
        return subscribers;
    }

//    public void update(LevelState levelState) {
//        notifyAllSubscribers(levelState);
//    }
}
