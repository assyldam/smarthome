package cz.cvut.fel.omo.semestralka.builders;

import cz.cvut.fel.omo.semestralka.configurations.HomeConfig;
import cz.cvut.fel.omo.semestralka.factories.RoomFactory;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;

/**
 * This class represents home builder.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class HomeBuilder{
    private Home home;
    private final RoomFactory roomFactory = new RoomFactory();

    public HomeBuilder() {
        reset();
    }

    private void reset(){
        this.home = Home.getInstance();
    }

    public Home build() {
        Home createdHome = home;
        reset();
        return createdHome;
    }

    public HomeBuilder createHome(HomeBuilder homeBuilder) {
        home = homeBuilder.build();
        return this;
    }

    public Home buildHomeFromConfig(HomeConfig homeConfig) {
        home.setPets(homeConfig.getPets());
        home.releaseItemToPool(homeConfig.getItems());
        home.releasePersonToPool(homeConfig.getPersons());
        for (Room room : roomFactory.createRoomsByConf(homeConfig.getRooms())) {
            home.addRoom(room);
        }
        home.getRooms().stream().flatMap(room -> room.getDevices().stream()).forEach(device -> home.releaseDeviceToPool(device));
        return build();
    }
}
