package cz.cvut.fel.omo.semestralka.enums;

public enum State {
    ON (true),
    OFF (false);

    private final boolean state;

    State(boolean state){
        this.state = state;
    }
}
