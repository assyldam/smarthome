package cz.cvut.fel.omo.semestralka.devicetypes.sensors;

import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.locations.StatsChangable;
import cz.cvut.fel.omo.semestralka.observers.HumidObserver;

public class SensorHumid extends Sensor {
    final private int lowestHumid = 20;
    final private int highestHumid = 50;

    /**update locationParams
     * @param location location(Room/Home)
     */
    @Override
    public void update(StatsChangable location) {
        observer.notifyAllSubscribers(getRightLevelState(location.getHumidity(), lowestHumid, highestHumid));
    }

    @Override
    public void setObserver() {
        this.observer = new HumidObserver();
    }

    public void setConsumption(){
        if (this.isOn)
            this.consumption = 0.1;
        else
            this.consumption = 0;

    }
}
