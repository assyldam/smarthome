package cz.cvut.fel.omo.semestralka.locations;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Window;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.visitor.ChangeRoomStateVisitor;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

import java.util.ArrayList;
import java.util.List;

public class Room extends StatsChangable{
    protected String name;
    protected int floor;
    protected List<Person> persons;
    protected List<Pet> pets;
    private double consumption;

    public double getConsumption() {
        return consumption;
    }

    private ChangeRoomStateVisitor changeRoomStateVisitor = new ChangeRoomStateVisitor(this);

    protected ArrayList<Device> devices = new ArrayList<>();
    protected ArrayList<Sensor> sensors = new ArrayList<>();

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public void accept(Visitor visitor){
        visitor.visit(this);
    }
    public Room() {
    }
    public Room(String name, int floor, List<Person> persons, List<Pet> pets) {
        this.name = name;
        this.floor = floor;
        this.persons = persons;
        this.pets = pets;
    }

    /**
     * Add devices to room
     * @param device Devices
     * @return Room
     */
    public Room addDevice(ArrayList<Device> device) {
        this.devices.addAll(device);
        return this;
    }

    public ArrayList<Sensor> getSensors() {
        return sensors;
    }

    /**
     * Add sensor to room
     * @param sensors Sensors
     * @return Room
     */
    public Room addSensors(ArrayList<Sensor> sensors){
        this.sensors.addAll(sensors);
        return this;
    }

    public Room setName(String name) {
        this.name = name;
        return this;
    }

    public ArrayList<Device> getDevices() {
        return devices;
    }

    public String getName() {
        return name;
    }

    public double getCurrentElectricityConsumption() {
        return consumption;
    }

    /**
     * Method, that update room state (Depends on Device's mod)
     */
    public void updateRoom(){
        for(Device device : devices){
            device.accept(changeRoomStateVisitor);
        }
    }

    public ArrayList<Window> getWindows() {
        ArrayList<Window> windows = new ArrayList<>();
        for (Device device : devices) {
            if (device instanceof Window) {
                windows.add((Window) device);
            }
        }
        return windows;
    }
}
