package cz.cvut.fel.omo.semestralka.animatedobjects;

/**
 * This class represents action.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class Action {
    protected boolean isFree = true;
    protected double actionTime;

    public void setFree(boolean free) {
        isFree = free;
    }
}
