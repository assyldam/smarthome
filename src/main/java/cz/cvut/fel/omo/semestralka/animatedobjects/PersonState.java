package cz.cvut.fel.omo.semestralka.animatedobjects;

/**
 * This enum represents person state.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public enum PersonState {
    Adult("Adult"), Child("Child");

    private final String name;

    /**
     * Constructor for PersonState.
     * @param name String
     */
    PersonState(String name) {
        this.name = name;
    }

    /**
     * This method creates String representation.
     */
    @Override
    public String toString() {
        return name;
    }
}
