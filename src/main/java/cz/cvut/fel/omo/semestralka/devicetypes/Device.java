package cz.cvut.fel.omo.semestralka.devicetypes;

import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate.LevelState;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate.LowLevelState;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate.MidLevelState;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate.PowerStateOff;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate.PowerStateOn;
import cz.cvut.fel.omo.semestralka.devicetypes.manual.Manual;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate.PowerState;
import cz.cvut.fel.omo.semestralka.enums.Devices;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

public abstract class Device {
    protected String name;
    protected Devices type;
    protected PowerState powerState = new PowerStateOff();
    protected boolean isFree;
    protected LevelState levelState = new LowLevelState();
    protected double defaultConsumption;
    protected int condition = 100;
    protected Manual manual;
    protected double consumption;

    public Device() {
        turnOff();
        this.levelState = new MidLevelState();
    }

    public Device(String name) {
        this.levelState = new MidLevelState();
        this.name = name;
    }

    public Device(String name, double consumption) {
        this(name);
        this.consumption = consumption;
        defaultConsumption = consumption;
    }

    public void turnOn() {
        this.powerState = new PowerStateOn();
    }

    public void turnOff() {
        this.powerState = new PowerStateOff();
    }

    public boolean isOn() {
        return powerState.isOn();
    }

    public void execute() {
        levelState.execute(this);
    }

    public void update() {
        powerState.execute(this);
    }

    /**
     * Set Device on High level
     */
    public void highLevelProcess(){
        condition -= 15;
        concreteHighLevelProcess();
    }

    /**
     * Set Device on Mid level
     */
    public void midLevelProcess(){
        condition -= 10;
        concreteMidLevelProcess();
    }

    /**
     * Set Device on Low level
     */
    public void lowLevelProcess(){
        condition -= 5;
        concreteLowLevelProcess();
    }

    /**
     * High level mode implementation
     */
    public abstract void concreteLowLevelProcess();

    /**
     * Mid level mode implementation
     */
    public abstract void concreteHighLevelProcess();

    /**
     * Low level mode implementation
     */
    public abstract void concreteMidLevelProcess();

    public abstract int accept(Visitor v);

    public boolean isFree() {
        return isFree;
    }

    public void setLevelState(LevelState levelState) {
        this.levelState = levelState;
    }

    public void setType(Devices type) {
        this.type = type;
    }

    public void setConsumption(double consumption) {
        defaultConsumption = consumption;
        this.consumption = consumption;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public void stopUsing() {
        isFree = true;
    }

    public void getFixed() {
        condition = 100;
        System.out.println(manual.getManText());
    }

    public abstract int getOutputPower();

    public String getName() {
        return this.name;
    }

    public double getConsumption() {
        return this.consumption;
    }

    public int getCondition() {
        return condition;
    }

    public String getCurrentState() {
        return getName() + " is " + this.powerState;
    }

    public Devices getType() {
        return type;
    }
}