package cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;

/**
 * This class represents low level state of device.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class LowLevelState implements LevelState {
    /**
     * Execute on Low mode
     * @param device Device
     */
    @Override
    public void execute(Device device){
        device.lowLevelProcess();
    }
}
