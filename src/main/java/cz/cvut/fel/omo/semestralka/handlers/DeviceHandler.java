package cz.cvut.fel.omo.semestralka.handlers;

import cz.cvut.fel.omo.semestralka.controller.Event;
import cz.cvut.fel.omo.semestralka.devicetypes.Fridge;

import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceHandler extends AbstractHandler {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceHandler.class);

    /**Main event logic here
     * @param event Event
     */
    @Override
    public void processEvent (Event event) {
        Random rand = new Random();
        setNextHandler(new PersonHandler());
        switch (event.getResponsibleDevice().getType()) {
            case Fridge -> {
                Fridge fridge = (Fridge) event.getResponsibleDevice();
                if(!fridge.getContent().isEmpty()) {
                    int randIndex = rand.nextInt(fridge.getContent().size());
                    String randomFoodFromFridge = fridge.getContent().get(randIndex);

                    if (event.getPet() != null) {
                        event.setWhatToDo("Use device " + event.getResponsibleDevice().getName() + " " +
                                event.getResponsibleDevice() + " " + randomFoodFromFridge + " " +
                                "to feed pet " + event.getPet().getName() + ".");
                    } else if (event.getChild() != null) {
                        event.setWhatToDo("Use device " + event.getResponsibleDevice().getName() + " " +
                                event.getResponsibleDevice() + " " + randomFoodFromFridge + " " +
                                "to feed child " + event.getChild().getName() + ".");

                    } else {

                        event.setWhatToDo("Use device " + event.getResponsibleDevice().getName() + " " +
                                event.getResponsibleDevice() + " " + randomFoodFromFridge +
                                " to eat.");

                    }
                    fridge.removeFood(randomFoodFromFridge);
                }
                else {
                    event.setWhatToDo(event.getResponsiblePerson().getName() + " have to find food in the kitchen, Fridge is empty");
                }
            }
            case Speaker -> {
                event.setWhatToDo("use Speaker");
                break;
            }

            case Camera -> {
                event.setWhatToDo("shoot something in camera");
                break;
            }
            default -> event.setWhatToDo("use device: " + event.getResponsibleDevice().getName());
        }
    }
}
