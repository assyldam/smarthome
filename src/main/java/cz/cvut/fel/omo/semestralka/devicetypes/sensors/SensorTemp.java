package cz.cvut.fel.omo.semestralka.devicetypes.sensors;

import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.locations.StatsChangable;
import cz.cvut.fel.omo.semestralka.observers.TempObserver;

public class SensorTemp extends Sensor{
    final private int lowestTemp = 19;
    final private int highestTemp = 23;

    /**update locationParams
     * @param location location(Room/Home)
     */
    @Override
    public void update(StatsChangable location) {
        observer.notifyAllSubscribers(getRightLevelState(location.getTemperature(), lowestTemp, highestTemp));
    }

    @Override
    public void setObserver() {
        this.observer = new TempObserver();
    }
}
