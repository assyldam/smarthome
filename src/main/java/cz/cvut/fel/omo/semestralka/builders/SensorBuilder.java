package cz.cvut.fel.omo.semestralka.builders;

import cz.cvut.fel.omo.semestralka.devicetypes.sensors.*;
import cz.cvut.fel.omo.semestralka.enums.Devices;

/**
 * This class represents sensor builder.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class SensorBuilder {
    private Sensor sensor;

    public SensorBuilder(){
        reset();
    }
    public void reset(){
        sensor = null;
    }

    public SensorBuilder setType(Devices devices){
        switch (devices){
            case HumidSensor -> sensor = new SensorHumid();
            case TempSensor -> sensor = new SensorTemp();
            case WindSensor -> sensor = new SensorWind();
            case LightSensor -> sensor = new SensorLight();
        }
        return this;
    }

    public SensorBuilder setConsumotion(double consumotion){
        sensor.setConsumption(consumotion);
        return this;
    }

    public SensorBuilder setObserver(){
        sensor.setObserver();
        return this;
    }

    public Sensor build(){
        Sensor tmp = sensor;
        reset();
        return tmp;
    }
}
