package cz.cvut.fel.omo.semestralka.facade;

import cz.cvut.fel.omo.semestralka.builders.HomeBuilder;
import cz.cvut.fel.omo.semestralka.configurations.ConfigurationLoader;
import cz.cvut.fel.omo.semestralka.configurations.HomeConfig;
import cz.cvut.fel.omo.semestralka.controller.Start;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.utilities.FinancesReportReportVisitor;
import cz.cvut.fel.omo.semestralka.utilities.HomeReportReportVisitor;

public class ClientAPI {
    HomeBuilder hb = new HomeBuilder();
    ConfigurationLoader loader = new ConfigurationLoader();

    /**
     * Starts simulation with first configuration.
     */
    public void startSimulation1() {
        HomeConfig hc = loader.loadConfiguration("src/main/resources/conf.json");
        Home h1 = hb.buildHomeFromConfig(hc);
        Start start1 = new Start(h1);
        start1.doRounds(20);
        createReports(start1.getHC().getHome());
    }

    /**
     * Starts simulation with second configuration.
     */
    public void startSimulation2() {
        HomeConfig hc = loader.loadConfiguration("src/main/resources/conf1.json");
        Home h2 = hb.buildHomeFromConfig(hc);
        Start start2 = new Start(h2);
        start2.doRounds(20);
        createReports(start2.getHC().getHome());
    }

    /**
     * Starts simulation with third configuration.
     */
    public void startSimulation3() {
        HomeConfig hc = loader.loadConfiguration("src/main/resources/conf2.json");
        Home h3 = hb.buildHomeFromConfig(hc);
        Start start3 = new Start(h3);
        start3.doRounds(20);
        createReports(start3.getHC().getHome());
    }

    /**
     * Creates reports.
     * @param home Home
     */
    private void createReports(Home home) {
        HomeReportReportVisitor homeReportVisitor = new HomeReportReportVisitor();
        homeReportVisitor.startReport();
        homeReportVisitor.visit(home);
        homeReportVisitor.endReport();

        FinancesReportReportVisitor financialReportVisitor = new FinancesReportReportVisitor();
        financialReportVisitor.startReport();
        financialReportVisitor.visit(home);
        financialReportVisitor.endReport();
    }
}
