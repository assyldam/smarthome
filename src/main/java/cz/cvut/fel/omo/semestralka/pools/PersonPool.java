package cz.cvut.fel.omo.semestralka.pools;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.PersonState;

import java.util.ArrayList;
import java.util.Collections;

public class PersonPool {
    private final ArrayList<Person> persons = new ArrayList<>();
    private static PersonPool INSTANCE;

    public static PersonPool getInstance(){
        if(INSTANCE != null) return INSTANCE;
        INSTANCE = new PersonPool();
        return INSTANCE;
    }

    /**
     * return back Person to pool
     * @param person Person
     */
    public void releasePerson(Person person){
        persons.add(person);
    }

    /**
     * return Person from pool
     * @return Person
     */
    public Person getRandomPerson(){
        if(!persons.isEmpty()) {
            Collections.shuffle(persons);
            Person tmp = persons.get(0);
            persons.remove(tmp);
            return tmp;
        }
        return null;
    }

    /**
     * return Adult from pool
     * @return Person
     */
    public Person getRandomAdult(){
        if(!persons.isEmpty()) {
            Collections.shuffle(persons);
            for(Person p : persons){
                if(p.getPersonState() == PersonState.Adult){
                    persons.remove(p);
                    return  p;
                }
            }

        }
        return null;
    }

    /**
     * return Child from pool
     * @return Person
     */
    public Person getRandomChild() {
        if (!persons.isEmpty()) {
            Collections.shuffle(persons);
            for (Person p : persons) {
                if (p.getPersonState() == PersonState.Child) {
                    persons.remove(p);
                    return p;
                }
            }

        }
        return null;
    }
}

