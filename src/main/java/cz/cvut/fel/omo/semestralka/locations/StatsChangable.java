package cz.cvut.fel.omo.semestralka.locations;

public abstract class StatsChangable {
    private int temperature;
    private int humidity;
    private int windSpeed;
    private int brightness;

    public int getTemperature() {
        return temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public int getBrightness() {
        return brightness;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }
}
