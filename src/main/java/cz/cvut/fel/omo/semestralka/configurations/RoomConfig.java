package cz.cvut.fel.omo.semestralka.configurations;

import java.util.ArrayList;

/**
 * This class represents room configuration.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class RoomConfig {
    private String name;
    private final ArrayList<DeviceConfig> devices;
    private final ArrayList<DeviceConfig> sensors = new ArrayList<>();

    public RoomConfig() {
        this.devices = new ArrayList<>();
    }


    public void addDevice(DeviceConfig deviceConfig) {
        this.devices.add(deviceConfig);
    }

    public void addSensor(DeviceConfig sensorConfig) {sensors.add(sensorConfig);}

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<DeviceConfig> getSensors() {
        return sensors;
    }

    public String getName() {
        return name;
    }

    public ArrayList<DeviceConfig> getDevices() {
        return devices;
    }
}
