package cz.cvut.fel.omo.semestralka.handlers;

import cz.cvut.fel.omo.semestralka.controller.Event;
import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class PersonHandler extends AbstractHandler {

    private static final Logger LOG = LoggerFactory.getLogger(PersonHandler.class);

    /**Main event logic here
     * @param event Event
     */
    @Override
    public void processEvent (Event event) {
        Person person = event.getResponsiblePerson();

        if (event.getChild() != null || event.getPet() != null) {

            if (event.getChild() != null) {
                LOG.info("{} did actions: be fed by " + person.getName() + ".", event.getChild().getName());
            }
            if (event.getPet() != null) {
                LOG.info("{} did actions: be fed by " + person.getName() + ".", event.getPet().getName());
            }
        }
        LOG.info("{} did actions: " + event.getWhatToDo(), person.getName());
    }
}
