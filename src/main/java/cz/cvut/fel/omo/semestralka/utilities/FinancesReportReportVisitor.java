package cz.cvut.fel.omo.semestralka.utilities;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.Window;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.items.Item;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FinancesReportReportVisitor implements Visitor {
    public static final Report consumptionReport = new Report("reports/FinancesAndConsumptionReport.txt");
    private final static DecimalFormat df2 = new DecimalFormat("#.##");

    public void startReport(){
        consumptionReport.writeToReport("――――――――――――――――――――――― REPORT BEGINNING ―――――――――――――――――――――――");
        consumptionReport.writeToReport("                        " + getDate()+ "                         ");
    }

    public void endReport(){
        consumptionReport.writeToReport("―――――――――――――――――――――――― REPORT END ――――――――――――――――――――――――");
        consumptionReport.endReport();
    }

    private String getDate(){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.M.yyyy hh:mm");
        return formatter.format(date);
    }

    private double calculatePriceOfConsumption(double e){
        double electricity = (e * 1.9) + e;
        return electricity ;
    }


    @Override
    public int doForAirConditioner(Device device) {
        return 0;
    }

    @Override
    public int doForHumidifier(Device device) {
        return 0;
    }

    @Override
    public int doForWindow(Device device) {
        return 0;
    }

    @Override
    public int doForBulb(Device device) {
        return 0;
    }

    @Override
    public int doForSpeaker(Device device) {
        return 0;
    }

    @Override
    public int doForDoor(Device device) {
        return 0;
    }

    @Override
    public int doForCamera(Device device) {
        return 0;
    }

    @Override
    public int doForFridge(Device device) {
        return 0;
    }

    @Override
    public void visit(Home home) {
        home.getRooms().forEach(
            room -> {
            room.getDevices().forEach(device -> {
                device.accept(new ConsumptionReportVisitor());
            });
            room.accept(new ConsumptionReportVisitor());
        });
        home.accept(new ConsumptionReportVisitor());
        double electricityForHome;
        double totalPriceForHome = 0;

        for (Room room : home.getRooms()) {
            electricityForHome = room.getCurrentElectricityConsumption();
            totalPriceForHome += calculatePriceOfConsumption(electricityForHome);
        }
        consumptionReport.writeToReport("House consumption: ");
        for (Room room : home.getRooms()) {
            visit(room);

            List<Device> devices = room.getDevices();
            for (Device device:devices) {
                visit(device);
            }
            List<Sensor> sensors = room.getSensors();
            for (Sensor sensor:sensors) {
                visit(sensor);
            }
        }
        consumptionReport.writeToReport("   Electricity :    " + df2.format(home.getConsumption()) );
        consumptionReport.writeToReport(" ―――――――――――――――――――――――――――――――");
        consumptionReport.writeToReport("   Total price:        " + df2.format(totalPriceForHome) + " Kč" + "\n");
    }

    @Override
    public void visit(Room room) {
        double e = room.getCurrentElectricityConsumption();
        double totalPrice = calculatePriceOfConsumption(e);

        consumptionReport.writeToReport("   -Room consumption and pricing for " + room.getName());
        consumptionReport.writeToReport("   Price for room:    " + df2.format(totalPrice) + " Kč" );
        consumptionReport.writeToReport("   Total electricity :    " + df2.format(e) + "  ═╗");
    }

    @Override
    public void visit(Device device) {
        double e = device.getConsumption();
        consumptionReport.writeToReport("           Consumption of device - " + device.getName());
        consumptionReport.writeToReport("           Electricity :    " + df2.format(e) );
        consumptionReport.writeToReport("\n");
    }

    @Override
    public void visit(Item item) {
    }

    @Override
    public void visit(Window window) {
    }

    @Override
    public void visit(Sensor sensor) {
        double e = sensor.getConsumption();
        consumptionReport.writeToReport("           Consumption of sensor - " + sensor.getName());
        consumptionReport.writeToReport("           Electricity :    " + df2.format(e) );
        consumptionReport.writeToReport("\n");
    }

    @Override
    public void visit(Person person) {
    }

    @Override
    public void visit(Pet pet) {
    }
}
