package cz.cvut.fel.omo.semestralka;

import cz.cvut.fel.omo.semestralka.facade.ClientAPI;

public class SmartHome {
    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {
        ClientAPI clientAPI = new ClientAPI();
//        clientAPI.startSimulation1();
        clientAPI.startSimulation2();
//        clientAPI.startSimulation3();
    }
}
