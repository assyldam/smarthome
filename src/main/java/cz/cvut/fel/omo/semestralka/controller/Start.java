package cz.cvut.fel.omo.semestralka.controller;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.locations.Home;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class represents Start.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class Start {
    private final HomeController HC;
    private static final Logger LOG = LoggerFactory.getLogger(Start.class);

    public Start(Home home){
        HC = new HomeController(home);
    }

    /**start smart home cycle
     * @param ROUNDS int
     */
    public void doRounds(int ROUNDS) {
        System.out.println();
        LOG.info("\n---------------- STARTING NEW SIMULATION ----------------");
        LOG.info("                     " + getDate()+ "                       \n");

        turnOnAllDevices();
        homeInfoBeforeSimulation();

        for (int roundNumber = 1; roundNumber <= ROUNDS; roundNumber++) {

            LOG.info("\n - ROUND " + roundNumber + " STARTED: -\n");

            prepareRoundAtStart();
            doRound(roundNumber);
            logRoomParameters();
        }
        System.out.println();
        LOG.info("\n--------------- END OF THE SIMULATION ----------------\n");
        System.out.println();
    }

    private String getDate(){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.M.yyyy hh:mm");
        return formatter.format(date);
    }

    private void homeInfoBeforeSimulation(){
        LOG.info("Rooms info: " +
                HC.getHome().getRooms().stream()
                        .flatMap(room -> Stream.concat(
                                room.getDevices().stream().map(device -> "\n" + "   " + room.getName() + " - " + device.getType() + " named " + device.getName() + " is free: " + device.isFree()),
                                room.getSensors().stream().map(sensor -> sensor.getName() + " (Sensor)"))
                        )
                        .collect(Collectors.joining(", ")) + "\n");
    }

    /**Handle one Round
     * @param roundNumber int
     */
    private void doRound(int roundNumber) {
        HC.updateHomeStatsParams();
        HC.reactOnAllEvents();
        if (roundNumber % 7 == 0) {
            HC.generateHungryEventForPet();

        }

        if (roundNumber % 3 == 0) {
            HC.generateHungryEventForChild();
            HC.generateHomeStatsChange();
        }

        if (roundNumber % 4 == 0) {
            HC.generateAndProcessHungryEventForAdult();
        }


        HC.generateEventsForFreePeople();
//        HC.reactOnAllEvents();
        HC.returnToHomeAllUsedEntity();
    }

    /**
     * Turn on all devices
     */
    private void turnOnAllDevices(){
//        ArrayList<Device> devices = new ArrayList<>();
//        while(true){
//            Device d = HC.getHome().getRandomDeviceFromPool();
//            if(d == null){
//                break;
//            }
//            devices.add(d);
//            d.turnOn();
//            System.out.println(d.getOutputPower() + " d.Name: " + d.getName() + " isOn: " +d.isOn());
//        }
//        devices.forEach(d -> HC.getHome().releaseDeviceToPool(d));
        HC.getHome().getRooms().forEach(room -> room.getDevices().forEach(Device::turnOn));
    }

    /**This method Log Room Parameters
     */
    private void logRoomParameters(){
        String logMessage = String.format("\n" + "Home parameters: -windspeed: %s, -temp: %s, -humid: %s, -brightness: %s",
                HC.getHome().getWindSpeed(),
                HC.getHome().getTemperature(),
                HC.getHome().getHumidity(),
                HC.getHome().getBrightness());
        LOG.info(logMessage);

    }

    /**Prepares the round at start.
     **/
    private void prepareRoundAtStart() {

        List<Device> brokenDevices = HC.findBrokenDevices();
        brokenDevices.forEach(brokenDevice -> {
            brokenDevice.setFree(false);
            Event event = new Event(HC.getHome());
            event.setResponsibleDevice(brokenDevice);
            event.setCause("Device is broken.");
            HC.getEvents().add(event);
        });
    }

    /** return Home Controller
     * @return HomeController
     */
    public HomeController getHC(){
        return HC;
    }
}
