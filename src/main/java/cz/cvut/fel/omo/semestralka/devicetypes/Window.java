package cz.cvut.fel.omo.semestralka.devicetypes;

import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate.PowerStateIdle;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate.PowerStateOff;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate.PowerStateOn;
import cz.cvut.fel.omo.semestralka.devicetypes.manual.Manual;
import cz.cvut.fel.omo.semestralka.enums.Code;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

public class Window extends Device {
    public Window(){
        super();
        manual = new Manual("Window manual)");
    }

    @Override
    public int getOutputPower() {
        return 0;
    }

    /**
     * High level mode implementation
     */
    @Override
    public void concreteHighLevelProcess() {
        powerState = new PowerStateOff();
        System.out.println(type + name + " works on high level mode");
    }

    /**
     * Mid level mode implementation
     */
    @Override
    public void concreteMidLevelProcess() {
        powerState = new PowerStateIdle();
        System.out.println(type + name + " works on mid level mode");
    }

    /**
     * Low level mode implementation
     */
    @Override
    public void concreteLowLevelProcess() {
        powerState = new PowerStateOn();
        System.out.println(type + name + " works on low level mode");
    }

    /**Method for visitor pattern
     */
    @Override
    public int accept(Visitor v) {
        return v.doForWindow(this);
    }

    @Override
    public void update() {
        execute();
    }
}
