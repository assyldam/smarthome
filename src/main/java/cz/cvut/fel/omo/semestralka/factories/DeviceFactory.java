package cz.cvut.fel.omo.semestralka.factories;

import cz.cvut.fel.omo.semestralka.builders.DeviceBuilder;
import cz.cvut.fel.omo.semestralka.configurations.DeviceConfig;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;

import java.util.ArrayList;

/**
 * This class represents device factory.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class DeviceFactory {
    private final DeviceBuilder deviceBuilder = new DeviceBuilder();

    public DeviceFactory(){
    }

    /**
     * Create one device
     * @param deviceConfig Device Config
     * @return Device
     */
    public Device createDevice(DeviceConfig deviceConfig){
        return deviceBuilder.setDeviceType(deviceConfig.getType()).
                setDeviceConsumption(deviceConfig.getConsumption()).
                setDeviceName(deviceConfig.getName()).
                build();
    }

    /**
     * Create List of devices
     * @param deviceConfigs Device Config
     * @return Devices list
     */
    public ArrayList<Device> createDevices(ArrayList<DeviceConfig> deviceConfigs){
        ArrayList<Device> devices = new ArrayList<>();
        for(DeviceConfig deviceConfig: deviceConfigs) {
            Device device = createDevice(deviceConfig);
            devices.add(device);
        }
        return devices;
    }


}
