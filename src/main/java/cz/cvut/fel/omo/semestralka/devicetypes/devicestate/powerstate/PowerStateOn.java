package cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.enums.Code;

public class PowerStateOn implements PowerState {

    @Override
    public boolean isOn(){
        return true;
    }

    /**Execute on "turn on" mode
     * @param device Device
     */
    @Override
    public void execute(Device device){
        device.execute();
    }
}
