package cz.cvut.fel.omo.semestralka.builders;

import cz.cvut.fel.omo.semestralka.devicetypes.*;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.enums.Devices;

/**
 * This class represents device builder.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class DeviceBuilder {
    private Device device;

    public DeviceBuilder(){
        reset();
    }

    private void reset(){
        this.device = null;

    }


    /**Set device based on type in config
     * @param type Devices(enum)
     * @return DeviceBuilder
     */
    public DeviceBuilder setDeviceType(Devices type){
        switch (type){
            case AirConditioner -> device = new AirConditioner();
            case Bulb -> device = new Bulb();
            case Camera -> device = new Camera();
            case Window -> device = new Window();
            case Door -> device = new Door();
            case Fridge -> device = new Fridge();
            case Speaker -> device = new Speaker();
            case Humidifier -> device = new Humidifier();
        }
        device.setType(type);
        return this;
    }

    public DeviceBuilder setDeviceConsumption(double consumption){
        device.setConsumption(consumption);
        return this;
    }

    public DeviceBuilder setDeviceName(String name){
        device.setName(name);
        return this;
    }

    public Device build() {
        Device device = this.device;
        reset();
        return device;
    }
}
