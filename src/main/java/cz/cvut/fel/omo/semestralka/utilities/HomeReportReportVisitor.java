package cz.cvut.fel.omo.semestralka.utilities;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.Window;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.items.Item;
import cz.cvut.fel.omo.semestralka.pools.ItemPool;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class HomeReportReportVisitor implements Visitor {
    private int counter = 1;
    public static final Report homeConfigReport = new Report("reports/HomeConfigReport.txt");

    public void startReport(){
        homeConfigReport.writeToReport("――――――――――――――――――――――― REPORT BEGINNING ―――――――――――――――――――――――");
        homeConfigReport.writeToReport("                        " + getDate()+ "                       ");
    }

    public void endReport(){
        homeConfigReport.writeToReport("―――――――――――――――――――――――― REPORT END ――――――――――――――――――――――――");
        homeConfigReport.endReport();
    }

    private String getDate(){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.M.yyyy hh:mm");
        return formatter.format(date);
    }


    @Override
    public int doForAirConditioner(Device device) {
        return 0;
    }

    @Override
    public int doForHumidifier(Device device) {
        return 0;
    }

    @Override
    public int doForWindow(Device device) {
        return 0;
    }

    @Override
    public int doForBulb(Device device) {
        return 0;
    }

    @Override
    public int doForSpeaker(Device device) {
        return 0;
    }

    @Override
    public int doForDoor(Device device) {
        return 0;
    }

    @Override
    public int doForCamera(Device device) {
        return 0;
    }

    @Override
    public int doForFridge(Device device) {
        return 0;
    }

    @Override
    public void visit(Home home) {
        homeConfigReport.writeToReport("Home ");
        int rooms = home.getRooms().size();
        for (int i = 0; i < rooms; i++){
            visit(home.getRooms().get(i));
        }

        homeConfigReport.writeToReport("\n");
        homeConfigReport.writeToReport("=== === === === === === === === === === === === ===");
        homeConfigReport.writeToReport("\n");

        ArrayList<Person> persons = new ArrayList<>();
        Person p;
        while(true){
            p = home.getRandomPerson();
            if(p == null) break;
            persons.add(p);
        }

        homeConfigReport.writeToReport("Number of occupants: " + persons.size());
        for (Person person : persons) {
            visit(person);
            home.releasePersonToPool(persons);
        }

        homeConfigReport.writeToReport("\n");
        homeConfigReport.writeToReport("=== === === === === === === === === === === === ===");
        homeConfigReport.writeToReport("\n");

        ArrayList<Pet> pets = home.getPets();
        homeConfigReport.writeToReport("Number of pets: " + pets.size());
        for (Pet pet : pets) {
            visit(pet);
        }

        homeConfigReport.writeToReport("\n");
        homeConfigReport.writeToReport("=== === === === === === === === === === === === ===");
        homeConfigReport.writeToReport("\n");

        ItemPool itemPool = ItemPool.getInstance();
        homeConfigReport.writeToReport("Number of items: " + itemPool.getItems().size());
        for (Item item : itemPool.getItems()) {
            visit(item);
        }

        homeConfigReport.writeToReport("\n");
    }

    @Override
    public void visit(Room room) {
        homeConfigReport.writeToReport("\n    Room " + "-" + room.getName() + "-" + " has");
        int devices = room.getDevices().size();
        for (int i = 0; i < devices; i++){
            visit(room.getDevices().get(i));
        }
        int sensors = room.getSensors().size();
        for (int i = 0; i < sensors; i++){
            visit(room.getSensors().get(i));
        }
    }

    @Override
    public void visit(Device device) {
        homeConfigReport.writeToReport("      Device " + device.getType() + " named " + device.getName());
    }



    @Override
    public void visit(Item item) {
        homeConfigReport.writeToReport("    Item " +
                item.getName());
    }


    @Override
    public void visit(Window window) {
        homeConfigReport.writeToReport("      Window " + counter);
    }

    @Override
    public void visit(Sensor sensor) {
        homeConfigReport.writeToReport("      Sensor " + sensor.getName());
    }

    @Override
    public void visit(Person person){
        int age = person.getAge();
        if (age == 1){
            homeConfigReport.writeToReport("    Hi, I'm " + person.getName() + " and I'm " + age + " year old.");
        } else {
            homeConfigReport.writeToReport("    Hi, I'm " + person.getName() + " and I'm " + age + " years old.");
        }
    }

    @Override
    public void visit(Pet pet) {
        homeConfigReport.writeToReport("    This is a " + pet.getPetType() + " named " + pet.getName() + ".");
    }
}
