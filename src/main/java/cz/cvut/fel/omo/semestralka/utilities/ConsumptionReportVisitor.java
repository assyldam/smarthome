package cz.cvut.fel.omo.semestralka.utilities;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.Window;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.items.Item;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

public class ConsumptionReportVisitor implements Visitor {

    @Override
    public int doForAirConditioner(Device device) {
        return 0;
    }

    @Override
    public int doForHumidifier(Device device) {
        return 0;
    }

    @Override
    public int doForWindow(Device device) {
        return 0;
    }

    @Override
    public int doForBulb(Device device) {
        return 0;
    }

    @Override
    public int doForSpeaker(Device device) {
        return 0;
    }

    @Override
    public int doForDoor(Device device) {
        return 0;
    }

    @Override
    public int doForCamera(Device device) {
        return 0;
    }

    @Override
    public int doForFridge(Device device) {
        return 0;
    }

    @Override
    public void visit(Home home) {
        double totalElectricity = 0;
        for(Room room : home.getRooms()){
            totalElectricity += room.getCurrentElectricityConsumption();
        }
        home.setConsumption(totalElectricity);
    }


    @Override
    public void visit(Room room) {
        double totalElectricity = 0;
        for(Device device : room.getDevices()){
            totalElectricity += device.getConsumption();
        }
        for (Sensor sensor : room.getSensors()){
            totalElectricity += sensor.getConsumption();
        }
        room.setConsumption(totalElectricity);
    }


    @Override
    public void visit(Device device) {
        device.setConsumption(device.getConsumption());
    }


    @Override
    public void visit(Item item) {

    }

    @Override
    public void visit(Person person) {

    }

    @Override
    public void visit(Pet pet) {

    }

    @Override
    public void visit(Window window) {

    }

    @Override
    public void visit(Sensor sensor) {
        sensor.setConsumption(sensor.getConsumption());
    }
}
