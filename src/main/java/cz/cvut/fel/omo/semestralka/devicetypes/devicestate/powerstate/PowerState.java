package cz.cvut.fel.omo.semestralka.devicetypes.devicestate.powerstate;

import cz.cvut.fel.omo.semestralka.enums.Code;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;

public interface PowerState {
    boolean isOn();

    void execute(Device device);
}
