package cz.cvut.fel.omo.semestralka.configurations;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.PersonState;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.enums.Devices;
import cz.cvut.fel.omo.semestralka.items.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This class represents configuration loader.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class ConfigurationLoader {

    public ConfigurationLoader() {

    }

    /**
     * parse config file and return container for Home values
     * @param filename String
     * @return HomeConfig <-class, that contain all home values(just container)
     */
    public HomeConfig loadConfiguration(String filename) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            JsonNode rootNode = objectMapper.readTree(new File(filename));
            if (rootNode.has("home")) {
                JsonNode homeNode = rootNode.get("home");
                HomeConfig homeConfig = buildHomeConfigFromNode(homeNode);
                return homeConfig;
            } else {
                System.err.println("Error: 'home' key not found in the configuration file.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Build Home Configuration from Json Node
     * @param homeNode JsonNode
     * @return HomeConfig <-class, that contain all home values(just container)
     */
    private HomeConfig buildHomeConfigFromNode(JsonNode homeNode) {
        HomeConfig homeConfig = new HomeConfig();

        if (homeNode.has("rooms") && homeNode.get("rooms").isArray()) {
            JsonNode roomsNode = homeNode.get("rooms");
            for (JsonNode roomNode : roomsNode) {
                homeConfig.addPersons(buildPersonsFromNode(roomNode));
                homeConfig.addItems(buildItemsFromNode(roomNode));
                homeConfig.addPets(buildPetsFromNode(roomNode));
                RoomConfig roomConfig = buildRoomConfigFromNode(roomNode);
                homeConfig.addRoom(roomConfig);
            }
        } else {
            System.err.println("Error: 'rooms' key not found or not an array in the configuration file.");
        }

        return homeConfig;
    }

    /**
     * Build Room Configuration from Json Node
     * @param roomNode JsonNode
     * @return HomeConfig <-class, that contain all home values(just container)
     */
    private RoomConfig buildRoomConfigFromNode(JsonNode roomNode) {
        RoomConfig roomConfig = new RoomConfig();

        if (roomNode.has("name") && roomNode.get("name").isTextual()) {
            roomConfig.setName(roomNode.get("name").asText());
        } else {
            System.err.println("Error: 'name' key not found or not a string in the room configuration.");
        }

        if (roomNode.has("devices") && roomNode.get("devices").isArray()) {
            JsonNode devicesNode = roomNode.get("devices");
            for (JsonNode deviceNode : devicesNode) {
                if (deviceNode.has("type") && deviceNode.has("name") &&
                        deviceNode.has("consumption") && deviceNode.has("state") && deviceNode.get("type").isTextual()){
                    Devices tmp = Devices.valueOf(deviceNode.get("type").asText());
                    switch (tmp) {
                        case AirConditioner, Bulb, Camera, Door, Fridge,
                                Speaker, Humidifier, Window-> {
                            DeviceConfig deviceConfig = new DeviceConfig(
                                deviceNode.get("name").asText(),
                                tmp,
                                deviceNode.get("consumption").asDouble(),
                                deviceNode.get("state").asBoolean());
                            roomConfig.addDevice(deviceConfig);
                        }
                        case HumidSensor, TempSensor, WindSensor, LightSensor -> {
                            DeviceConfig sensorConfig = new DeviceConfig( deviceNode.get("name").asText(),
                                    tmp,
                                    deviceNode.get("consumption").asDouble(),
                                    deviceNode.get("state").asBoolean());
                            roomConfig.addSensor(sensorConfig);
                        }
                    }
                } else {
                    System.err.println("Error: 'type' key not found or not a string in the device configuration.");
                }
            }
        } else {
            System.err.println("Error: 'devices' key not found or not an array in the room configuration.");
        }

        return roomConfig;
    }
    /**
     * Build Items from Json Node
     * @param roomNode JsonNode
     * @return HomeConfig <-class, that contain all home values(just container)
     */
    private ArrayList<Item> buildItemsFromNode(JsonNode roomNode){
        ArrayList<Item> itemList = new ArrayList<>();
        if (roomNode.has("items") && roomNode.get("items").isArray()) {
            JsonNode itemsNode = roomNode.get("items");
            for (JsonNode itemNode : itemsNode) {
                if (itemNode.has("type") && itemNode.get("type").isTextual()) {
                    Item item;
                    switch (itemNode.get("type").asText()){
                        case "Bike" -> {
                            item = new Bike();
                            item.setType("Bike");
                            itemList.add(item);
                        }
                        case "Skis" -> {
                            item = new Skis();
                            item.setType("Skis");
                            itemList.add(item);
                        }
                        case "Car" -> {
                            item = new Car();
                            item.setType("Car");
                            itemList.add(item);
                        }
                        case "JumpingRope" -> {
                            item = new JumpingRope();
                            item.setType("JumpingRope");
                            itemList.add(item);
                        }
                    }
                } else {
                    System.err.println("Error: 'type' key not found or not a string in the items configuration.");
                }
            }
        } else {
            System.err.println("Error: 'items' key not found or not an array in the room configuration.");
        }
        return itemList;
    }
    /**
     * Build Pets from Json Node
     * @param roomNode JsonNode
     * @return HomeConfig <-class, that contain all home values(just container)
     */
    private ArrayList<Pet> buildPetsFromNode(JsonNode roomNode){
        ArrayList<Pet> petList = new ArrayList<>();
        if (roomNode.has("pets") && roomNode.get("pets").isArray()) {
            JsonNode petsNode = roomNode.get("pets");
            for (JsonNode petNode : petsNode) {
                if (petNode.has("name") && petNode.get("name").isTextual() && petNode.get("type").isTextual()) {
                    Pet pet = new Pet(petNode.get("name").asText());
                    pet.setPetType(petNode.get("type").asText());
                    petList.add(pet);
                    //roomConfig.addPet(new Pet(petNode.get("name").asText()));
                }
            }
        }
        return petList;
    }
    /**
     * Build Persons from Json Node
     * @param roomNode JsonNode
     * @return HomeConfig <-class, that contain all home values(just container)
     */
    private ArrayList<Person> buildPersonsFromNode(JsonNode roomNode){
        ArrayList<Person> personList = new ArrayList<>();
        if (roomNode.has("people") && roomNode.get("people").isArray()) {
            JsonNode personsNode = roomNode.get("people");
            for (JsonNode personNode : personsNode) {
                if (personNode.has("name") && personNode.get("name").isTextual() && personNode.has("age") && personNode.get("age").isInt()) {
                    PersonState personState = PersonState.valueOf(personNode.get("state").asText());
                    Person person = new Person(personNode.get("name").asText(), personState);
                    person.setAge(personNode.get("age").asInt());
                    personList.add(person);
                    //roomConfig.addPerson(new Person(personNode.get("name").asText(), personState));
                }
            }
        }
        return personList;
    }

 }
