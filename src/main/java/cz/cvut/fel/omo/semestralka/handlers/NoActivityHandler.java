package cz.cvut.fel.omo.semestralka.handlers;

import cz.cvut.fel.omo.semestralka.controller.Event;
import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.locations.Room;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NoActivityHandler extends AbstractHandler {

    private static final Logger LOG = LoggerFactory.getLogger(NoActivityHandler.class);

    /**Main event logic here
     * @param event Event
     */
    @Override
    public void processEvent (Event event) {
        Person person = event.getResponsiblePerson();
        person.setRoom(changeRoom(event));
        LOG.info("{} is waiting in the " + person.getRoom().getName() + ".", person.getName());
    }

    public Room changeRoom(Event event) {
        List<Room> rooms = event.getHome().getRooms();
        Collections.shuffle(rooms);
        return rooms.get(0);
    }
}
