package cz.cvut.fel.omo.semestralka.configurations;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.items.Item;

import java.util.ArrayList;

/**
 * This class represents home configuration.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class HomeConfig {
    private final ArrayList<RoomConfig> rooms;
    private final ArrayList<Person> persons = new ArrayList<>();
    private final ArrayList<Item> items = new ArrayList<>();
    private final ArrayList<Pet> pets = new ArrayList<>();

    public HomeConfig() {
        this.rooms = new ArrayList<>();
    }

    public void addRoom(RoomConfig room) {
        this.rooms.add(room);
    }

    public void addPersons(ArrayList<Person> persons){this.persons.addAll(persons);}

    public void addItems(ArrayList<Item> items){this.items.addAll(items);}

    public void addPets(ArrayList<Pet> pets){this.pets.addAll(pets);}

    public ArrayList<RoomConfig> getRooms() {
        return rooms;
    }

    public ArrayList<Person> getPersons(){
        return persons;
    }

    public ArrayList<Item> getItems(){
        return items;
    }

    public ArrayList<Pet> getPets(){
        return pets;
    }

}
