package cz.cvut.fel.omo.semestralka.animatedobjects;

/**
 * This enum represents person activity.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public enum PersonActivity {

    doSport("do sport"),
    useDevice("use device");

    private final String name;

    PersonActivity(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
