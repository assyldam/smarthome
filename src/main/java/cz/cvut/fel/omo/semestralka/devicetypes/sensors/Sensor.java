package cz.cvut.fel.omo.semestralka.devicetypes.sensors;

import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate.HighLevelState;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate.LevelState;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate.LowLevelState;
import cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate.MidLevelState;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.locations.StatsChangable;
import cz.cvut.fel.omo.semestralka.observers.Observer;

public abstract class Sensor{
    protected boolean isOn;

    protected double consumption;

    protected Observer observer;


    /**Calculate and return Right level mode for devices
     * @param roomData RoomData
     * @param lowestValue lowest value
     * @param highestValue highest value
     * @return LevelState (devices will set this level as current)
     */
    protected LevelState getRightLevelState(double roomData, double lowestValue, double highestValue){
        if (roomData > highestValue){
            return new HighLevelState();
        }
        else if (roomData < lowestValue){
            return new LowLevelState();
        }
        else{
            return new MidLevelState();
        }
    }

    /**update locationParams
     * @param location location(Room/Home)
     */
    public abstract void update(StatsChangable location);

    public boolean isOn() {
        return this.isOn;
    }

    public void setConsumption(double consumption){
        this.consumption = consumption;
    }

    public abstract void setObserver();

    public Observer getObserver() {
        return observer;
    }

    public String getName() {
        return this.getClass().getSimpleName();
    }

    public double getConsumption() {
        return consumption;
    }
}
