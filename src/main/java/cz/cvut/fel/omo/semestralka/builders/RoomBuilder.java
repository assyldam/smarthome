package cz.cvut.fel.omo.semestralka.builders;

import cz.cvut.fel.omo.semestralka.configurations.RoomConfig;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.factories.DeviceFactory;
import cz.cvut.fel.omo.semestralka.factories.SensorFactory;
import cz.cvut.fel.omo.semestralka.locations.Room;

import java.util.ArrayList;

/**
 * This class represents room builder.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class  RoomBuilder {
    private Room room;
    private ArrayList<Device> devices = new ArrayList<>();
    private DeviceFactory deviceFactory = new DeviceFactory();
    private SensorFactory sensorFactory = new SensorFactory();

    public RoomBuilder() {
        reset();
    }

    private void reset(){
        this.room = new Room();
    }

    public RoomBuilder setName(String name) {
        this.room.setName(name);
        return this;
    }

    public RoomBuilder addDevice(RoomConfig roomConf) {
        room.addSensors(sensorFactory.createSensor(roomConf.getSensors()));
        room.addDevice(deviceFactory.createDevices(roomConf.getDevices()));
        for(Sensor sensor: room.getSensors()){
            sensor.getObserver().addSubscribers(room.getDevices());
        }
        return this;
    }

    public Room build() {
        Room createdRoom = room;
        reset();
        return createdRoom;
    }
}
