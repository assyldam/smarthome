package cz.cvut.fel.omo.semestralka.enums;

public enum Devices {
    AirConditioner ("AirConditioner"),
    Bulb ("Bulb"),
    Camera ("Camera"),
    Door ("Door"),
    Fridge ("Fridge"),
    Humidifier ("Humidifier"),
    HumidSensor ("HumidSensor"),
    TempSensor ("TempSensor"),
    WindSensor ("WindSensor"),
    LightSensor ("LightSensor"),
    Speaker ("Speaker"),
    Window ("Window");
    Devices(String s) {
    }
}
