package cz.cvut.fel.omo.semestralka.handlers;

import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;

public class ParameterGenerator {

    public ParameterGenerator() {
    }

    public void changeTemperature(Home home){
        home.setTemperature(generateRanValue(10, 30));
    }

    public void changeWindSpeed(Home home){
        home.setWindSpeed(generateRanValue(1, 20));
    }

    public void changeHumidity(Home home){
        home.setHumidity(generateRanValue(20, 80));
    }

    public void changeBrightnessAtRoom(Home home){
        home.setBrightness(generateRanValue(0, 100));
    }

    public void changeTemperature(Room room){
        room.setTemperature(generateRanValue(10, 30));
    }

    public void changeWindSpeed(Room room){
        room.setWindSpeed(generateRanValue(1, 20));
    }

    public void changHumidity(Room room){
        room.setHumidity(generateRanValue(20, 80));
    }

    public void changeBrightnessAtRoom(Room room){
        room.setBrightness(generateRanValue(0, 100));
    }
    private int generateRanValue(int min, int max){
        return (int) (Math.random() * (max - min + 1)) + min;
    }
}