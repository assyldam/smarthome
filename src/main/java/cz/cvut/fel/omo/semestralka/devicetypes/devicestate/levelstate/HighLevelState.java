package cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;

/**
 * This class represents high level state of device.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class HighLevelState implements LevelState {
    /**
     * execute on high level mode
     * @param device Device
     */
    @Override
    public void execute(Device device){
        device.highLevelProcess();
    }
}
