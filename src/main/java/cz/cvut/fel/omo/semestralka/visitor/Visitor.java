package cz.cvut.fel.omo.semestralka.visitor;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.Window;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.items.Item;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;

public interface Visitor {
    int doForAirConditioner(Device device);

    int doForHumidifier(Device device);

    int doForWindow(Device device);

    int doForBulb(Device device);

    int doForSpeaker(Device device);

    int doForDoor(Device device);

    int doForCamera(Device device);

    int doForFridge(Device device);

    void visit(Home home);

    void visit(Room room);

    void visit(Device device);

    void visit(Item item);

    void visit(Person person);

    void visit(Pet pet);

    void visit(Window window);

    void visit(Sensor sensor);
}
