package cz.cvut.fel.omo.semestralka.items;

import cz.cvut.fel.omo.semestralka.animatedobjects.Action;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

public abstract class Item {
    private String type;
    protected String name;

    protected Item(){
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getName() {
        return name;
    }

    /**
     * This method a part of visitor impl
     * @param visitor Visitor
     */
    public void accept(Visitor visitor){
        visitor.visit(this);
    }

    public String toString() {
        return "Item: " + name;
    }

}