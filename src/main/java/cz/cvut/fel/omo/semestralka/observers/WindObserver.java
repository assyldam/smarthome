package cz.cvut.fel.omo.semestralka.observers;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.enums.Devices;

import java.util.ArrayList;

public class WindObserver extends Observer{

    /**
     * Add Subscribers to Observer
     * @param devices Devices
     */
    @Override
    public void addSubscribers(ArrayList<Device> devices) {
        for(Device device: devices){
            if(device.getType() == Devices.Window){
                subscribers.add(device);
            }
        }
    }
}
