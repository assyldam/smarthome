package cz.cvut.fel.omo.semestralka.handlers;


import cz.cvut.fel.omo.semestralka.controller.Event;

public abstract class AbstractHandler {

    protected AbstractHandler nextHandler;

    /**Handle Event, start next event
     *
     * @param event Event
     */
    public void handleEvent(Event event){
        if (event != null) {
            processEvent(event);
        }

        if (nextHandler != null) {
            nextHandler.handleEvent(event);
        }
    }

    /**
     * main event logic here
     * @param event Event
     */
    public void processEvent (Event event) {}

    public void setNextHandler(AbstractHandler nextHandler) {
        this.nextHandler = nextHandler;
    }
}
