package cz.cvut.fel.omo.semestralka.devicetypes;
import cz.cvut.fel.omo.semestralka.devicetypes.manual.Manual;
import cz.cvut.fel.omo.semestralka.enums.Code;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

public class Bulb extends Device {
    private int brightness;

    public Bulb(){
        super();
        manual = new Manual("Bulb manual");
        this.brightness = 0;
    }

    @Override
    public int getOutputPower() {
        return brightness;
    }

    @Override
    public String getCurrentState() {
        if (isOn()) {
            return getName() + " is " + this.powerState + " with brightness " + this.brightness;
        } else {
            return getName() + " is " + this.powerState;
        }
    }

    /**
     * High level mode implementation
     */
    @Override
    public void concreteHighLevelProcess() {
        consumption = defaultConsumption * 1.5;
        brightness = 0;
        System.out.println(type + " " + name + " works on high level mode with consumption: " + consumption);
    }

    /**
     * Mid level mode implementation
     */
    @Override
    public void concreteMidLevelProcess() {
        brightness = 5;
        consumption = defaultConsumption;
        System.out.println(type + " " + name + " works on mid level mode with consumption: " + consumption);
    }

    /**
     * Low level mode implementation
     */
    @Override
    public void concreteLowLevelProcess() {
        brightness = 10;
        consumption = defaultConsumption / 2;
        System.out.println(type + " " + name + " works on low level mode with consumption: " + consumption);
    }

    /**Method for visitor pattern
     */
    @Override
    public int accept(Visitor v) {
        return v.doForBulb(this);
    }
}
