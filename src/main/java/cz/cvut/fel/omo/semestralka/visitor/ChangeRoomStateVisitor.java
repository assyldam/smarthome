package cz.cvut.fel.omo.semestralka.visitor;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.Window;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.items.Item;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.locations.StatsChangable;

public class ChangeRoomStateVisitor implements Visitor{

    private StatsChangable location;

    public ChangeRoomStateVisitor(StatsChangable location){
        this.location = location;
    }

    @Override
    public int doForAirConditioner(Device device) {
        location.setTemperature(location.getTemperature() + device.getOutputPower());
        return 0;
    }

    @Override
    public int doForHumidifier(Device device) {
        location.setHumidity(location.getHumidity() - device.getOutputPower());
        return 0;
    }

    @Override
    public int doForWindow(Device device) {
        if(!device.isOn()) location.setWindSpeed(0);
        return 0;
    }

    @Override
    public int doForBulb(Device device) {
        location.setBrightness(location.getBrightness() + device.getOutputPower());
        return 0;
    }

    @Override
    public int doForSpeaker(Device device) {
        return 0;
    }

    @Override
    public int doForDoor(Device device) {
        return 0;
    }

    @Override
    public int doForCamera(Device device) {
        return 0;
    }

    @Override
    public int doForFridge(Device device) {
        return 0;
    }

    @Override
    public void visit(Home home) {
    }

    @Override
    public void visit(Room room) {

    }

    @Override
    public void visit(Device device) {

    }

    @Override
    public void visit(Item item) {

    }

    @Override
    public void visit(Person person) {

    }

    @Override
    public void visit(Pet pet) {

    }

    @Override
    public void visit(Window window) {

    }

    @Override
    public void visit(Sensor sensor) {

    }
}
