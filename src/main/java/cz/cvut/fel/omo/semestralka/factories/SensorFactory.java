package cz.cvut.fel.omo.semestralka.factories;

import cz.cvut.fel.omo.semestralka.builders.SensorBuilder;
import cz.cvut.fel.omo.semestralka.configurations.DeviceConfig;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;

import java.util.ArrayList;

/**
 * This class represents sensor factory.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class SensorFactory {
    private SensorBuilder sensorBuilder = new SensorBuilder();
    public SensorFactory(){
    }

    /**
     * Create List of Sensors
     * @param sensorConfigs Sensor Configs
     * @return Sensors
     */
    public ArrayList<Sensor> createSensor(ArrayList<DeviceConfig> sensorConfigs){
        ArrayList<Sensor> sensors = new ArrayList<>();
        for(DeviceConfig sensorConfig : sensorConfigs){
            sensors.add(sensorBuilder.setType(sensorConfig.getType()).setConsumotion(sensorConfig.getConsumption()).setObserver().build());
        }
        return sensors;
    }
}
