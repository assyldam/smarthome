package cz.cvut.fel.omo.semestralka.devicetypes;

import cz.cvut.fel.omo.semestralka.devicetypes.manual.Manual;
import cz.cvut.fel.omo.semestralka.enums.Code;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

public class Humidifier extends Device {
    private int humidityOutput = 0;
    public Humidifier(){
        super();
        manual = new Manual("Humidifier manual");
    }

    @Override
    public int getOutputPower() {
        return humidityOutput;
    }

    /**
     * High level mode implementation
     */
    @Override
    public void concreteHighLevelProcess() {
        consumption = defaultConsumption * 1.5;
        humidityOutput = 2;
        System.out.println(type + name + " works on high level mode with consumption: " + consumption);
    }

    /**
     * Mid level mode implementation
     */
    @Override
    public void concreteMidLevelProcess() {
        consumption = defaultConsumption;
        humidityOutput = 0;
        System.out.println(type + name + " works on mid level mode with consumption: " + consumption);
    }

    /**
     * Low level mode implementation
     */
    @Override
    public void concreteLowLevelProcess() {
        humidityOutput = -2;
        consumption = defaultConsumption / 2;
        System.out.println(type + name + " works on low level mode with consumption: " + consumption);
    }

    /**Method for visitor pattern
     */
    @Override
    public int accept(Visitor v) {
        return v.doForHumidifier(this);
    }
}
