package cz.cvut.fel.omo.semestralka.devicetypes;

import cz.cvut.fel.omo.semestralka.devicetypes.manual.Manual;
import cz.cvut.fel.omo.semestralka.enums.Code;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

public class Speaker extends Device {
    public Speaker(){
        super();
        manual = new Manual("Speaker manual");
    }

    @Override
    public int getOutputPower() {
        return 0;
    }

    /**
     * High level mode implementation
     */
    @Override
    public void concreteHighLevelProcess() {
        System.out.println(type + name + " works on high level mode");
    }

    /**
     * Mid level mode implementation
     */
    @Override
    public void concreteMidLevelProcess() {
        System.out.println(type + name + " works on mid level mode");
    }

    /**
     * Low level mode implementation
     */
    @Override
    public void concreteLowLevelProcess() {
        System.out.println(type + name + " works on low level mode");
    }

    /**Method for visitor pattern
     */
    @Override
    public int accept(Visitor v) {
        return v.doForSpeaker(this);
    }
}
