package cz.cvut.fel.omo.semestralka.enums;

public enum Code {
    HIGHLEVEL,
    MEDIUMLEVEL,
    LOWLEVEL
}
