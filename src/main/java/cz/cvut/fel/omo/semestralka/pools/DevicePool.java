package cz.cvut.fel.omo.semestralka.pools;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;

import java.util.ArrayList;
import java.util.Collections;

public class DevicePool {
    private final ArrayList<Device> devices = new ArrayList<>();

    private static DevicePool INSTANCE;

    public DevicePool(){
    }

    public static DevicePool getInstance(){
        if(INSTANCE != null) {
            return INSTANCE;
        }
        INSTANCE = new DevicePool();
        return INSTANCE;
    }

    /**
     * return back Device to pool
     * @param device Device
     */
    public void releaseDevice(Device device){
        devices.add(device);
    }

    /**
     * return Device from pool
     * @return Device
     */
    public Device getRandomDevice(){
        if(!devices.isEmpty()) {
            Collections.shuffle(devices);
            Device tmp = devices.get(0);
            devices.remove(tmp);
            return tmp;
        }
        return null;
    }

}
