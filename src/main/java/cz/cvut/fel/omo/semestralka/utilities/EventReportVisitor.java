package cz.cvut.fel.omo.semestralka.utilities;

import cz.cvut.fel.omo.semestralka.handlers.ParameterGenerator;
import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.PersonActivity;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.Window;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.items.Item;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

import java.util.Random;

public class EventReportVisitor implements Visitor {
    private Random rand = new Random();
    private ParameterGenerator parameterGenerator;

    public EventReportVisitor(){
        parameterGenerator = new ParameterGenerator();
    }

    @Override
    public void visit(Device device) {

        if (device.getCurrentState().toString().equals("OFF")) {
            device.turnOn();
            useDevice(device);
            device.turnOff();

            return;
        }
        useDevice(device);
    }


    public void useDevice(Device device) {
        device.execute();
        device.setFree(false);

        device.accept(new ConsumptionReportVisitor());

//        device.setCondition(device.getCondition() - 30);
//        device.notifyAllObservers();

        device.stopUsing();
    }

    @Override
    public void visit(Item item) {
        //item.setFree(false);

    }

    @Override
    public void visit(Person person) {
        PersonActivity[] values = PersonActivity.values();
        int length = values.length;
        int randIndex = rand.nextInt(length);

        //person.setFree(false);
        person.setActivity(values[randIndex]);
    }

    @Override
    public void visit(Pet pet) {
        pet.setHungry(true);
    }


    @Override
    public int doForAirConditioner(Device device) {
        if(!device.isOn()){
            device.turnOn();
        }
        return 0;
    }

    @Override
    public int doForHumidifier(Device device) {
        return 0;
    }

    @Override
    public int doForWindow(Device device) {
        return 0;
    }

    @Override
    public int doForBulb(Device device) {
        return 0;
    }

    @Override
    public int doForSpeaker(Device device) {
        return 0;
    }

    @Override
    public int doForDoor(Device device) {
        return 0;
    }

    @Override
    public int doForCamera(Device device) {
        return 0;
    }

    @Override
    public int doForFridge(Device device) {
        return 0;
    }

    @Override
    public void visit(Home home) {
        parameterGenerator.changeBrightnessAtRoom(home);
        parameterGenerator.changeHumidity(home);
        parameterGenerator.changeTemperature(home);
        parameterGenerator.changeWindSpeed(home);
    }

    @Override
    public void visit(Window window) {

    }

    @Override
    public void visit(Sensor sensor) {

    }

    @Override
    public void visit(Room room) {

    }
}
