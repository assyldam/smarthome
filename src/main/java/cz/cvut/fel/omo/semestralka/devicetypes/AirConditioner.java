package cz.cvut.fel.omo.semestralka.devicetypes;

import cz.cvut.fel.omo.semestralka.devicetypes.manual.Manual;
import cz.cvut.fel.omo.semestralka.enums.Code;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

public class AirConditioner extends Device {
    private int outputPower;
    public AirConditioner(){
        super();
        outputPower = 0;
        manual = new Manual("AirConditioner manual");
    }

    @Override
    public int getOutputPower() {
        return outputPower;
    }


    /**
     * High level mode implementation
     */
    @Override
    public void concreteHighLevelProcess() {
        outputPower = -5;
        consumption = defaultConsumption * 1.5;
        System.out.println(type + name + " works on high level mode with consumption: " + consumption);
    }

    /**
     * Mid level mode implementation
     */
    @Override
    public void concreteMidLevelProcess() {
        outputPower = 0;
        consumption = defaultConsumption;
        System.out.println(type + " - " + name + " | works on mid level mode with consumption: " + consumption);
    }

    /**
     * Low level mode implementation
     */
    @Override
    public void concreteLowLevelProcess() {
        outputPower = 5;
        consumption = defaultConsumption / 2;
        System.out.println(type + " - " + name + " | works on low level mode with consumption: " + consumption);
    }

    /**Method for visitor pattern
     */
    @Override
    public int accept(Visitor v) {
        v.doForAirConditioner(this);
        return 0;
    }
}
