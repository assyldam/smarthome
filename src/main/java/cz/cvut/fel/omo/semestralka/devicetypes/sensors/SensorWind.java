package cz.cvut.fel.omo.semestralka.devicetypes.sensors;

import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.locations.StatsChangable;
import cz.cvut.fel.omo.semestralka.observers.WindObserver;

public class SensorWind extends Sensor{
    final private int lowestSpeed = 10;
    final private int highestSpeed = 20;

    /**update locationParams
     * @param location location(Room/Home)
     */
    @Override
    public void update(StatsChangable location) {
        observer.notifyAllSubscribers(getRightLevelState(location.getWindSpeed(), lowestSpeed, highestSpeed));
    }

    @Override
    public void setObserver() {
        this.observer = new WindObserver();
    }
}
