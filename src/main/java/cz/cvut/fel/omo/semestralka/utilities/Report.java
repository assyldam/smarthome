package cz.cvut.fel.omo.semestralka.utilities;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;

import static java.util.logging.Logger.*;

public class Report {

    private FileOutputStream log;

    public Report(String name) {
        try {
            log = new FileOutputStream(name, false);
        } catch (FileNotFoundException ex) {
            getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void writeToReport(String text) {
        text += System.lineSeparator();
        try {
            log.write(text.getBytes());
        } catch (IOException ex) {
            getLogger(HomeReportReportVisitor.class.getName()).log(Level.SEVERE, null, ex);
            getLogger(FinancesReportReportVisitor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void endReport() {
        try {
            log.close();
        } catch (IOException ex) {
            getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}