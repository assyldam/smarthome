package cz.cvut.fel.omo.semestralka.factories;

import cz.cvut.fel.omo.semestralka.builders.RoomBuilder;
import cz.cvut.fel.omo.semestralka.configurations.DeviceConfig;
import cz.cvut.fel.omo.semestralka.configurations.RoomConfig;
import cz.cvut.fel.omo.semestralka.locations.Room;

import java.util.ArrayList;

/**
 * This class represents room factory.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class RoomFactory {
    private RoomBuilder roomBuilder = new RoomBuilder();
    public RoomFactory() {
        super();
    }

    /**
     * Create one room
     * @param roomConfig Room Configuration
     * @return Room
     */
    public Room createRoom(RoomConfig roomConfig) {
        roomBuilder.addDevice(roomConfig).setName(roomConfig.getName());
        return roomBuilder.build();
    }

    /**
     * Create List of Rooms
     * @param roomConfigs RoomConfigs
     * @return Rooms
     */
    public ArrayList<Room> createRoomsByConf(ArrayList<RoomConfig> roomConfigs){
        ArrayList<Room> rooms = new ArrayList<>();
        for (RoomConfig roomConfig : roomConfigs) {
            rooms.add(createRoom(roomConfig));
        }
        return rooms;
    }
}
