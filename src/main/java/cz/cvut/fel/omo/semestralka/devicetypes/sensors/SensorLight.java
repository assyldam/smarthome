package cz.cvut.fel.omo.semestralka.devicetypes.sensors;

import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.locations.Room;
import cz.cvut.fel.omo.semestralka.locations.StatsChangable;
import cz.cvut.fel.omo.semestralka.observers.LightObserver;

public class SensorLight extends Sensor{
    final private int lowestBrightness = 30;
    final private int highestBrightness = 60;

    /**update locationParams
     * @param location location(Room/Home)
     */
    @Override
    public void update(StatsChangable location) {
        observer.notifyAllSubscribers(getRightLevelState(location.getBrightness(), lowestBrightness, highestBrightness));
    }

    @Override
    public void setObserver() {
        this.observer = new LightObserver();
    }
}
