package cz.cvut.fel.omo.semestralka.devicetypes.devicestate.levelstate;

import cz.cvut.fel.omo.semestralka.devicetypes.Device;

/**
 * This class represents mid level state of device.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class MidLevelState implements LevelState {

    /**Execute on Mid level mode
     * @param device Device
     */
    @Override
    public void execute(Device device) {
        device.midLevelProcess();
    }
}
