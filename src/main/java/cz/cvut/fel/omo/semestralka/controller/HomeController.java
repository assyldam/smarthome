package cz.cvut.fel.omo.semestralka.controller;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.PersonState;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.devicetypes.Fridge;
import cz.cvut.fel.omo.semestralka.devicetypes.sensors.Sensor;
import cz.cvut.fel.omo.semestralka.enums.Devices;
import cz.cvut.fel.omo.semestralka.items.*;
import cz.cvut.fel.omo.semestralka.handlers.*;
import cz.cvut.fel.omo.semestralka.locations.Home;
import cz.cvut.fel.omo.semestralka.utilities.EventReportVisitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.management.InstanceNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static cz.cvut.fel.omo.semestralka.animatedobjects.PersonActivity.useDevice;


/**
 * This class represents home controller.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class HomeController {
    private final ArrayList<Event> events;

    private final Home home;

    private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

    private final EventReportVisitor eventReportVisitor;

    private ArrayList<Device> usedDevices;

    private ArrayList<Item> usedItems;

    private ArrayList<Person> usedPersons;

    private ArrayList<Pet> usedPet;
    public HomeController(Home home){
        eventReportVisitor = new EventReportVisitor();
        events = new ArrayList<>();
        this.home = home;
        usedDevices = new ArrayList<>();
        usedItems = new ArrayList<>();
        usedPersons = new ArrayList<>();
        usedPet = new ArrayList<>();
    }

    /** Change stats of home(temperature, wind speed, humidity, brightness) */
    public void generateHomeStatsChange(){
        home.accept(eventReportVisitor);
        Event event = new Event(home);
        event.setCause("Home params has changed");
        events.add(event);
    }

    /**Handle case, where person can't use device neither item
     * @param event Event
     */
    private void doNoActivity(Event event) {
        NoActivityHandler noActivityHandler = new NoActivityHandler();
        noActivityHandler.handleEvent(event);
    }

    /**Search Activity.
     * if the sport activity was not found, then the device activity is searched
     * @param event Event
     */
    private void doSportActivity(Event event){
        try {
            findSportActivity(event);
            return;

        } catch (InstanceNotFoundException e) {
            LOG.info(e.getMessage());

        }

        try {
            findDeviceActivity(event);
            return;

        } catch (InstanceNotFoundException e) {
            LOG.info(e.getMessage());

        }
        doNoActivity(event);
    }

    /** Process Sport/Item Activity
     * @param event Event
     * @throws InstanceNotFoundException
     */
    private void findSportActivity(Event event) throws InstanceNotFoundException {
        Item item = home.getItemFromPool();
        if(item != null) {
            usedItems.add(item);
            if (item.getType().equals("Bike")) {

                Bike bike = (Bike) item;
                bike.accept(eventReportVisitor);
                event.setResponsibleItem(bike);

                ItemHandler itemHandler = new ItemHandler();
                itemHandler.handleEvent(event);
                return;
            }

            if ( item.getType().equals("Skis") &&
                    event.getResponsiblePerson().getPersonState().equals(PersonState.Adult)) {

                Skis skis = (Skis) item;
                skis.accept(eventReportVisitor);
                event.setResponsibleItem(skis);

                ItemHandler itemHandler = new ItemHandler();
                itemHandler.handleEvent(event);
                return;
            }

            if (item.getType().equals("JumpingRope") &&
                    event.getResponsiblePerson().getPersonState().equals(PersonState.Adult)) {

                JumpingRope jumpingRope = (JumpingRope) item;
                jumpingRope.accept(eventReportVisitor);
                event.setResponsibleItem(jumpingRope);

                ItemHandler itemHandler = new ItemHandler();
                itemHandler.handleEvent(event);
                return;
            }
            if (item.getType().equals("Car") &&
                    event.getResponsiblePerson().getPersonState().equals(PersonState.Adult)) {

                Car jumpingRope = (Car) item;
                jumpingRope.accept(eventReportVisitor);
                event.setResponsibleItem(jumpingRope);

                ItemHandler itemHandler = new ItemHandler();
                itemHandler.handleEvent(event);
                return;
            }
        }

        throw new InstanceNotFoundException("Sport activity was not found, trying to find another activity for" +
                " person.");
    }
    /**Search Activity.
     * if the device activity was not found, then the sport activity is searched.
     * in case
     * @param event Event
     */
    private void doDeviceActivity(Event event) {

        try {
            findDeviceActivity(event);
            return;

        } catch (InstanceNotFoundException e) {
            LOG.info(e.getMessage());

        }

        try {
            findSportActivity(event);
            return;

        } catch (InstanceNotFoundException e) {
            LOG.info(e.getMessage());
        }
        doNoActivity(event);
    }


    /**
     * Find free Device and put it in event
     * @param event Event
     * @throws InstanceNotFoundException devices out of stock
     */
    private void findDeviceActivity(Event event) throws InstanceNotFoundException {

        Device device = home.getRandomDeviceFromPool();

        if (device != null) {
            usedDevices.add(device);
            device.accept(eventReportVisitor);
            event.getResponsiblePerson().setActivity(useDevice);
            event.setResponsibleDevice(device);
            DeviceHandler deviceHandler = new DeviceHandler();
            deviceHandler.handleEvent(event);
            return;
        }
        throw new InstanceNotFoundException("Device activity was not found, trying to find another activity for " +
                "person.");
    }

    /**
     * Generate event for one person
     * @param person Person
     */
    private void generateEventForFreePerson(Person person) {
        person.accept(eventReportVisitor);

        Event event = new Event(home);
        event.setResponsiblePerson(person);
        usedPersons.add(person);

        switch(person.getActivity()) {
            case doSport -> doSportActivity(event);
            case useDevice -> doDeviceActivity(event);
            default -> throw new IllegalArgumentException("Unknown event reason!");
        }


        event.setCause(person.getActivity().toString());
        //events.add(event);
    }

    /**This method generates event for free people.
     **/
    public void generateEventsForFreePeople() {
        ArrayList<Person> freePeople = new ArrayList<>();
        while(true){
            Person p = home.getRandomPerson();
            if(p == null){
                break;
            }
            freePeople.add(p);
        }

        freePeople.forEach(this::generateEventForFreePerson);
    }


    /**This method generates event for hungry child.
     * @
     **/
    public void generateHungryEventForChild() {
        Person child = home.getRandomChild();
        //child.setHungry(true);
        //child.setFree(false);
        usedPersons.add(child);

        Event event = new Event(home);
        event.setChild(child);
        event.setCause("Child is hungry.");
        events.add(event);
        LOG.info("Child {} did actions: become hungry.", child.getName());
    }

    /**This method generates event for hungry pet.
     *
     **/
    public void generateHungryEventForPet() {
        ArrayList<Pet> pets = home.getPets();
        if(pets.isEmpty()){
            return;
        }
        Pet pet = pets.get(0);
        pet.accept(eventReportVisitor);
        usedPet.add(pet);

        Event event = new Event(home);
        pet.setHungry(true);
        event.setPet(pet);
        event.setCause("Pet is hungry.");
        events.add(event);

        LOG.info("Pet {} did actions: become hungry.", pet.getName());
    }

    /**This method generates event for hungry adult and feeds the adult.
     *
     **/
    public void generateAndProcessHungryEventForAdult() {
        Person p = home.getRandomAdult();
        if (p == null){
            return;
        }
        usedPersons.add(p);
        Event event = new Event(home);
        event.setResponsiblePerson(p);
        event.setCause("Person " + p.getName() + " is hungry.");

        Optional<Device> f = home.getRooms().stream().flatMap(room -> room.getDevices().stream()).filter(device ->
                device.getType() == Devices.Fridge).findFirst();

        if (f.isPresent()) {
            Fridge fridge = (Fridge) f.get();
            if (!fridge.getContent().isEmpty()) {

                event.setResponsibleDevice(fridge);
                DeviceHandler devH = new DeviceHandler();
                devH.handleEvent(event);

            }
            else {
                reactByGettingFoodFromKitchenForAdult(event);
            }
        } else {
            reactByGettingFoodFromKitchenForAdult(event);
        }

    }

    /**This method reacts on event hungry pet.
     * @param  event Event
     **/
    private void reactByGettingFoodFromKitchenForPet(Event event) {

        event.setWhatToDo("Feed pet " + event.getPet().getName() + " with some food in the kitchen.");
        PersonHandler p = new PersonHandler();
        p.handleEvent(event);

    }

    /**This method reacts on event hungry child.
     * @param  event Event
     **/
    private void reactByGettingFoodFromKitchenForChild(Event event) {

        event.setWhatToDo("Feed child " + event.getChild().getName() + " with some food in the kitchen.");
        PersonHandler p = new PersonHandler();
        p.handleEvent(event);

    }

    /**This method reacts on event hungry adult.
     * @param  event Event
     **/
    private void reactByGettingFoodFromKitchenForAdult(Event event) {

        event.setWhatToDo("Eat some food found in the kitchen.");
        PersonHandler p = new PersonHandler();
        p.handleEvent(event);

    }

    /**This method reacts on event for person.
     * @param  event Event
     * @param  person Person
     **/
    public void reactOnEvent(Event event, Person person) {

        event.setResponsiblePerson(person);

        switch(event.getCause()) {

            case "Pet is hungry."-> {

                event.getPet().setHungry(false);

                Optional<Device> fridge = event.getHome().getRooms().stream().flatMap(room -> room.getDevices().stream()).filter(device ->
                        device.getType() == Devices.Fridge).findFirst();

                if (fridge.isPresent()) {

                    Fridge fridge1 = (Fridge) fridge.get();

                    if (!fridge1.getContent().isEmpty()) {

                        event.setResponsibleDevice(fridge1);
                        event.setWhatToDo("feed pet " + event.getPet().getName() + ".");

                        DeviceHandler d = new DeviceHandler();
                        d.handleEvent(event);

                    } else {
                        reactByGettingFoodFromKitchenForPet(event);
                    }
                } else {
                    reactByGettingFoodFromKitchenForPet(event);
                }
            }
            case "Child is hungry." -> {

                Optional<Device> f = event.getHome().getRooms().stream().flatMap(room -> room.getDevices().stream()).filter(device ->
                        device.getType() == Devices.Fridge).findFirst();

                if (f.isPresent()) {

                    Fridge fridge2 = (Fridge) f.get();

                    if (!fridge2.getContent().isEmpty()) {

                        event.setResponsibleDevice(fridge2);
                        DeviceHandler deviceHandler = new DeviceHandler();
                        deviceHandler.handleEvent(event);
                    } else {
                        reactByGettingFoodFromKitchenForChild(event);
                    }

                } else {
                    reactByGettingFoodFromKitchenForChild(event);
                }
            }
            case "Home params has changed" -> {

                ArrayList<Sensor> s = home.getSensors();

                if (!s.isEmpty()) {
                    s.forEach(sensor -> sensor.update(home));
                    home.getRooms().stream().flatMap(room -> room.getDevices().stream()).forEach(Device::update);
                }


            }
            case "Device is broken." -> {
                LOG.info("{} device is broken.", event.getResponsibleDevice().getName());
                event.setWhatToDo("repair the " + event.getResponsibleDevice().getName() + ".");

                PersonHandler personHandler = new PersonHandler();
                personHandler.setNextHandler(new BrokenDeviceHandler());
                personHandler.handleEvent(event);

            }
            default ->
                throw new IllegalArgumentException("Unknown event reason!");
        }



    }

    /**This method reacts on all events.
     **/
    public void reactOnAllEvents() {
        ArrayList<Person> freeAdults = new ArrayList<>();
        while(true){
            Person p = home.getRandomAdult();
            if(p == null){
                break;
            }
            freeAdults.add(p);
        }
        for(Event e: events){
            if(e.getCause().equals("Home params has changed")){
                reactOnEvent(e, null);
            }
        }
        int lastIndex = 0;
        for (int i = 0; i < Math.min(freeAdults.size(), events.size()); i++) {
            reactOnEvent(events.get(i), freeAdults.get(i));
            lastIndex = i;
        }
        freeAdults.forEach(home::releasePersonToPool);
        if (!events.isEmpty()) {
            events.subList(0, lastIndex + 1).clear();
        }

    }

    /**
     * return all used People/Items/Devices in pools in Home
     */
    public void returnToHomeAllUsedEntity(){
        usedPersons.forEach(home::releasePersonToPool);
        usedItems.forEach(home::releaseItemToPool);
        usedDevices.forEach(home::releaseDeviceToPool);
        usedItems.clear();
        usedDevices.clear();
        usedPersons.clear();
    }

    public void returnToHomeAllUsedEntity2(){
        getUsedPersons().forEach(home::releasePersonToPool);
        getUsedItems().forEach(home::releaseItemToPool);
        getUsedDevices().forEach(home::releaseDeviceToPool);
        getUsedItems().clear();
        getUsedDevices().clear();
        getUsedPersons().clear();
    }

    public ArrayList<Device> getUsedDevices() {
        return usedDevices;
    }

    public ArrayList<Item> getUsedItems() {
        return usedItems;
    }

    public ArrayList<Person> getUsedPersons() {
        return usedPersons;
    }

    public ArrayList<Pet> getUsedPet() {
        return usedPet;
    }

    /**This method returns list of broken devices.
     * @return List<Device>
     **/
    public ArrayList<Device> findBrokenDevices() {
        return home.getRooms().stream()
                .flatMap(room -> room.getDevices().stream())
                .filter(device -> device.getCondition() <= 0)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**This method returns home instance.
     *
     **/
    public Home getHome() {
        return home;
    }

    /**This method returns list of events.
     * @return List<Event>
     **/
    public List<Event> getEvents() {
        return events;
    }

    public void updateHomeStatsParams(){
        home.updateHomeStats();
    }
}

