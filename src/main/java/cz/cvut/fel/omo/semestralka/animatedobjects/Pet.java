package cz.cvut.fel.omo.semestralka.animatedobjects;

import cz.cvut.fel.omo.semestralka.enums.PetType;
import cz.cvut.fel.omo.semestralka.visitor.Visitor;

/**
 * This class represents pet.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class Pet {
    protected boolean isHungry = false;
    private String name;
    private PetType petType;
    public Pet(String name) {
        this.name = name;
    }

    public void setHungry(boolean hungry) {
        isHungry = hungry;
    }

    public PetType getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = PetType.valueOf(petType);
    }

    public String getName() {
        return name;
    }

    /**Performs visitor logic for Pet class
     * @param visitor Visitor
     */
    public void accept(Visitor visitor){
        visitor.visit(this);
    }
}
