package cz.cvut.fel.omo.semestralka.controller;

import cz.cvut.fel.omo.semestralka.animatedobjects.Person;
import cz.cvut.fel.omo.semestralka.animatedobjects.Pet;
import cz.cvut.fel.omo.semestralka.devicetypes.Device;
import cz.cvut.fel.omo.semestralka.items.Item;
import cz.cvut.fel.omo.semestralka.locations.Home;

/**
 * This class represents event.
 *
 * @author Kabi Damir, Nurakhmetov Alisher
 */
public class Event {
    private String cause;
    private String whatToDo;
    private final Home home;
    private Person responsiblePerson;
    private Device responsibleDevice;
    private Item responsibleItem;
    private Pet pet;
    private Person child;

    public Event(Home home) {
        this.home = home;
    }

    public String getCause() {
        return cause;
    }

    public String getWhatToDo() {
        return whatToDo;
    }

    public void setWhatToDo(String whatToDo) {
        this.whatToDo = whatToDo;
    }

    public Home getHome() {
        return home;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public Person getResponsiblePerson() {
        return responsiblePerson;
    }

    public void setResponsiblePerson(Person responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }

    public Device getResponsibleDevice() {
        return responsibleDevice;
    }

    public void setResponsibleDevice(Device responsibleDevice) {
        this.responsibleDevice = responsibleDevice;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Person getChild() {
        return child;
    }

    public void setChild(Person child) {
        this.child = child;
    }

    public Item getResponsibleItem(){
        return responsibleItem;
    }

    public void setResponsibleItem(Item responsibleItem) {
        this.responsibleItem = responsibleItem;
    }
}
